<?php

namespace Drupal\flashpoint_experimental\Plugin\flashpoint_course;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\flashpoint_course\FlashpointCourseRendererInterface;
use Drupal\flashpoint_course\FlashpointCourseUtilities;
use Drupal\group\Entity\Group;

/**
 * @FlashpointCourseRenderer(
 *   id = "ajax_list_course",
 *   label = @Translation("Experimental (AJAXListCourse): Use an AJAX-enabled, Bootstrap friendly, list with content area."),
 * )
 */
class AjaxListCourse extends PluginBase implements FlashpointCourseRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Renders course using the default method.');
  }

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function rebuild($form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if (isset($trigger['#flashpoint_course_content_id'])) {
      $id = $trigger['#flashpoint_course_content_id'];
      $entity_type = 'flashpoint_course_content';
      $view_mode = 'full';
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
      $node = $storage->load($id);
      $build = $view_builder->view($node, $view_mode);
      $output = render($build);

      $add['activity_area'] = [
        '#id' => 'activity_area',
        '#type' => 'container',
        '#attributes' => [
          'id' => ['edit-activity-area'],
          'class' => ['col-md-9 col-md-push-3']
        ],
      ];
      $add['activity_area']['course_description'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $output
      ];
    }

    return $add;
  }

  /**
   * Gets an array of Flashpoint Access plugin definitions.
   * @return mixed
   */
  public static function getAccessPluginDefinitions() {
    $plugin_manager = \Drupal::service('plugin.manager.flashpoint_access');
    return $plugin_manager->getDefinitions();
  }

  /**
   * @param $course
   * @return array
   */
  public static function renderCourse($course) {
    $course = Group::load($course);
    $description = $course->get('field_course_description')->getValue();
    $description = isset($description[0]['value']) ? $description[0]['value'] : '';
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $account = \Drupal::currentUser();

    $form = [];

    /*
     * Determine the access and render modes.
     *
     * Note that if someone is enrolled, they already have view access.
     * If they have view access, they already have preview access.
     * Enroll access = preview access, no view access, and not enrolled.
     */

    // Get access types
    $enroll_access = FlashpointCourseUtilities::enrollAccess($course, $account);
    $view_access = FlashpointCourseUtilities::viewAccess($course, $account);
    $preview_access = $view_access || $enroll_access ? TRUE : FlashpointCourseUtilities::previewAccess($course, $account);

    // Determine the access mode. If FALSE, the user does not have access.
    $access_mode = $preview_access ? 'preview' : FALSE;
    $access_mode = $view_access ? 'view' : $access_mode;

    // Content mode - course only, course + content, or course, modules, and content
    $moduleHandler = \Drupal::service('module_handler');
    $content_mode = 'course';
    // Course modules also require content. There is no content-only mode.
    if ($moduleHandler->moduleExists('flashpoint_course_module') && $moduleHandler->moduleExists('flashpoint_course_content')) {
      $content_mode = 'course_module_content';
    }
    // Course and content.
    elseif ($moduleHandler->moduleExists('flashpoint_course_content')) {
      $content_mode = 'course_content';
    }

    // If the person may view or preview the course, render it.
    if ($access_mode) {
      $add = [];
      switch ($content_mode) {
        case 'course':
          // The course doesn't have any content, so just show the description and enroll options.
          $form['description'] = [
            '#type' => 'markup',
            '#markup' => $description,
          ];
          // Enroll access means the user is not enrolled, but is eligible.
          if ($enroll_access) {
            $plugin_definitions = AjaxListCourse::getAccessPluginDefinitions();
            $form['enroll_access_options'] = AjaxListCourse::renderJoinOptions($course, $flashpoint_config, $plugin_definitions);
          }
          break;

        case 'course_content':
          // Build the basic structure.
          $form = AjaxListCourse::renderBaseArea($description, $course, $flashpoint_config, $enroll_access);
          if ($view_access) {
            $add = AjaxListCourse::renderCourseWithoutModules($course, $flashpoint_config, $access_mode);
          }
          break;

        case 'course_module_content':
          // Build the basic structure.
          $form = AjaxListCourse::renderBaseArea($description, $course, $flashpoint_config, $enroll_access);
          $add = AjaxListCourse::renderCourseWithModules($course, $flashpoint_config, $access_mode);
          if (empty($add)) {
            $add = AjaxListCourse::renderCourseWithoutModules($course, $flashpoint_config, $access_mode);
          }
          break;
      }
      foreach ($add as $key => $value) {
        $form['content_list'][$key] = $value;
      }
    }

    $form['#attached']['library'][] = 'flashpoint_course/ajax_course';

    return $form;
  }

  /**
   * Builds the basic layout of the listing page. Content is added to this framework.
   *
   * @param string $description
   *   Course description.
   * @param $course
   *   Drupal Group Entity
   * @param $flashpoint_config
   *   Configuration object with Flashpoint settings.
   * @param bool $enroll_access
   *   Enroll Access - Means the person is not enrolled, but eligible.
   * @return array
   *   Form API formatted array.
   *
   * @see \Drupal\group\Entity\Group
   */
  public static function renderBaseArea($description, $course, $flashpoint_config, $enroll_access) {
    $add = [];
    $add['activity_area'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['col-md-9 col-md-push-3']
      ],
    ];
    $add['content_list'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['bg-default col-md-3 col-md-pull-9']
      ],
    ];
    // Course Description.
    $add['activity_area']['course_description']['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $description,
    ];
    // Enroll access means the user is not enrolled, but is eligible.
    if ($enroll_access) {
      $plugin_definitions = AjaxListCourse::getAccessPluginDefinitions();
      $add['activity_area']['course_description']['view_access_options'] = AjaxListCourse::renderViewOptions($course, $flashpoint_config, $plugin_definitions);
      $add['activity_area']['course_description']['enroll_access_options'] = AjaxListCourse::renderJoinOptions($course, $flashpoint_config, $plugin_definitions);
    }
    return $add;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @param $access_mode
   * @return array
   */
  public static function renderCourseWithModules(Group $course, $flashpoint_config, $access_mode) {
    $args = [$course->id()];
    $add = [];

    $module_view = FlashpointCourseUtilities::getCourseModuleView($course);
    if (!empty($module_view->result)) {
      foreach ($module_view->result as $result) {
        $module = $result->_entity;
        $module = $module->getEntity();

        if ($module->isPublished()) {
          $add['module_' . $module->id()] = [
            '#type' => 'html_tag',
            '#tag' => 'h2',
            '#value' => $module->label(),
            '#attributes' => ['class' => ['flashpoint-course-module']],
          ];
          /* @var $module \Drupal\flashpoint_course_module\Entity\FlashpointCourseModule */
          $content_set = $module->getCourseContent(FALSE, TRUE);
          foreach ($content_set as $cs) {
            foreach ($cs as $content) {
              switch ($access_mode) {
                case 'preview':
                  $add['module_content_' . $content->id()] = $add['module_content_' . $content->id()] = $content->renderListing(FALSE, 'lock');
                  break;
                case 'view':
                  $add['module_content_' . $content->id()] = $content->renderListing();
                  break;
              }
            }
          }
        }
      }
    }

    return $add;
  }

  /**
   * If no modules are present, or flashpoint_course_module is not enabled, run this.
   *
   * @param Group $course
   * @param $flashpoint_config
   * @param $access_mode
   * @return array
   */
  public static function renderCourseWithoutModules(Group $course, $flashpoint_config, $access_mode) {
    $args = [$course->id()];
    $add = [];

    $content_view = FlashpointCourseUtilities::getCourseContentView($course);

    if (!empty($content_view->result)) {
      foreach ($content_view->result as $result) {
        $content = $result->_entity;
        // In some cases $result->_entity is what we wanted. If not, use this.
        if (method_exists($content, 'getEntity')) {
          $content = $content->getEntity();
        }
        switch ($access_mode) {
          case 'preview':
            $add['content_' . $content->id()] = $content->renderListing(FALSE,'lock');
            break;
          case 'view':
            $add['content_' . $content->id()] = $content->renderListing();
            $add['content_' . $content->id()]['#ajax'] = [
              'callback' => 'Drupal\flashpoint_experimental\Plugin\flashpoint_course\AjaxListCourse::rebuild',
              'disable-refocus' => FALSE,
              'event' => 'click',
              'wrapper' => 'edit-activity-area',
              'progress' => [
                'type' => 'throbber',
                'message' => t('Loading Content...'),
              ],
            ];
            break;
        }
      }
    }

    return $add;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @param $plugin_definitions
   * @return array
   */
  public static function renderViewOptions(Group $course, $flashpoint_config, $plugin_definitions) {
    $element = [];
    $view_plugins = $course->get('field_view_access')->getValue();
    $view_access_form_add = [];
    foreach ($view_plugins as $view_plugin) {
      $vp = $view_plugin['value'];
      $vp_array = $plugin_definitions[$vp]['class']::viewAccessFormElement($course, \Drupal::currentUser());
      $view_access_form_add = !empty($vp_array) ? array_merge($view_access_form_add, $vp_array) : $view_access_form_add;
    }
    if (!empty($view_access_form_add)) {
      $view_label = $flashpoint_config->getOriginal('flashpoint_course.view_access_label');
      $view_label = !empty($view_label) ? $view_label : t('Get Access');
      $element = [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $view_label,
        ]
      ];
      $element = array_merge($element, $view_access_form_add);
    }
    return $element;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @param $plugin_definitions
   * @return array
   */
  public static function renderJoinOptions(Group $course, $flashpoint_config, $plugin_definitions) {
    $element = [];
    $enroll_plugins_and = $course->get('field_enrollment_conditions_and')->getValue();
    $enroll_plugins_or = $course->get('field_enrollment_conditions_or')->getValue();
    $enroll_label = $flashpoint_config->getOriginal('flashpoint_course.enroll_access_label');
    $enroll_label = !empty($enroll_label) ? $enroll_label : t('Enroll');
    $enroll_and_access_form_add = [];
    $enroll_or_access_form_add = [];


      foreach ($enroll_plugins_and as $enroll_plugin) {
        $ep = $enroll_plugin['value'];
        $ep_array = $plugin_definitions[$ep]['class']::joinAccessFormElement($course, \Drupal::currentUser());
        $enroll_and_access_form_add = !empty($ep_array) ? array_merge($enroll_and_access_form_add, $ep_array) : $enroll_and_access_form_add;
      }
      foreach ($enroll_plugins_or as $enroll_plugin) {
        $epor = $enroll_plugin['value'];
        $epor_array = $plugin_definitions[$epor]['class']::joinAccessFormElement($course, \Drupal::currentUser());
        $enroll_or_access_form_add = !empty($epor_array) ? array_merge($enroll_or_access_form_add, $epor_array) : $enroll_or_access_form_add;
      }
    if (\Drupal::currentUser()->isAnonymous() && (!empty($enroll_and_access_form_add) || !empty($enroll_or_access_form_add))) {
      $element = [
        '#type' => 'html_tag',
        '#weight' => 1,
        '#tag' => 'h3',
        '#value' => t('Please log in to see enrollment options'),
      ];
      $enroll_and_access_form_add = [];
      $enroll_or_access_form_add = [];
    }

    if (!empty($enroll_and_access_form_add) || !empty($enroll_or_access_form_add)) {
      $element = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $enroll_label,
      ];
      if (!empty($enroll_and_access_form_add)) {
        $element['enroll_access_and'] = [
          'header_and' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => '',
            'enroll_access_and_text' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('All of the following conditions must be met.'),
            ],
          ],
        ];
        $element['enroll_access_and'] = array_merge($element['enroll_access_and'], $enroll_and_access_form_add);
      }
      if (!empty($enroll_or_access_form_add)) {
        $element['enroll_access_or'] = [
          'header_or' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => '',
            'enroll_access_or_text' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('At least one of the following conditions must be met.'),
            ],
          ],
        ];
        $element['enroll_access_or'] = array_merge($element['enroll_access_or'], $enroll_or_access_form_add);
      }
    }
    return $element;
  }

}
