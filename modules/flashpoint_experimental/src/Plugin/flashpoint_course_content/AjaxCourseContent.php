<?php

namespace Drupal\flashpoint_experimental\Plugin\flashpoint_course_content;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\flashpoint_course\FlashpointCourseUtilities;
use Drupal\flashpoint_course_content\FlashpointCourseContentRendererInterface;
use Drupal\flashpoint_course_content\FlashpointCourseContentUtilities;
use Drupal\flashpoint_course_content\Plugin\flashpoint_course_content\FlashpointCourseContentDefaultRenderer;

/**
 * @FlashpointCourseContentRenderer(
 *   id = "ajax_listing_content",
 *   label = @Translation("AJAX content renderer that pairs with the AJAXListCourse plugin"),
 * )
 */
class AjaxCourseContent extends FlashpointCourseContentDefaultRenderer {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Default Renderer. Shows items as a list of divs with a set of specified classes.');
  }

  /**
   * @param $bundle
   * @param string $type
   * @return array
   */
  public static function getContentRenderData($bundle, $type = 'class') {
    return FlashpointCourseContentDefaultRenderer::getContentRenderData($bundle, $type);
  }

  /**
   * Render course content in a listing context.
   *
   * @param $content
   * @param $account
   * @param bool|string $force_status
   * @return array
   */
  public static function renderListing($content, $account, $force_status = FALSE) {
    $classes = FlashpointCourseContentDefaultRenderer::getContentRenderData($content->bundle(), 'class');
    $icons = FlashpointCourseContentDefaultRenderer::getContentRenderData($content->bundle(), 'icon');
    $status = FlashpointCourseContentUtilities::getCourseContentStatus($content, $account, $force_status);


    $content_label = $content->label();

    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['flashpoint-content', $classes[$status]],
      ],
      'icon-wrapper' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['flashpoint-content-icon-wrapper', $classes[$status]],
        ],
        'icon' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $icons[$status],
          '#attributes' => [
            'class' => ['flashpoint-content-icon'],
          ],
        ],
      ],
      'label_wrapper' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => ['flashpoint-content-label-wrapper', $classes[$status]],
        ],
        'label' => [
          '#type' => 'button',
          '#value' => $content->label(),
          '#attributes' => [
            'class' => ['flashpoint-content-label'],
          ],
          '#flashpoint_course_content_id' => $content->id(),
          '#ajax' => [
            'callback' => 'Drupal\flashpoint_experimental\Plugin\flashpoint_course\AjaxListCourse::rebuild', // don't forget :: when calling a class method.
            'disable-refocus' => TRUE,
            'event' => 'click',
            'wrapper' => 'edit-activity-area', // This element is updated with this AJAX callback.
          ]
        ],
      ],
    ];
  }

  /**
   * @param $content
   * @param $course
   * @return array
   */
  public function renderProgressButtons($content, $course) {
    return [];
  }

}
