<?php

namespace Drupal\flashpoint_course_module;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for course_module.
 */
class FlashpointCourseModuleTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
