<?php
/**
 * @file
 */

namespace Drupal\flashpoint_course_module\Controller;

use Drupal\flashpoint_course_module\FlashpointCourseModuleRendererManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FlashpointCourseModuleRendererController
 *
 * Provides the route and API controller for flashpoint_course_module.
 */
class FlashpointCourseModuleRendererController extends ControllerBase
{

  protected $FlashpointCourseModuleRendererManager; //The plugin manager.

  /**
   * Constructor.
   *
   * @param FlashpointCourseModuleRendererManager $plugin_manager
   */

  public function __construct(FlashpointCourseModuleRendererManager $plugin_manager) {
    $this->FlashpointCourseModuleRendererManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Use the service container to instantiate a new instance of our controller.
    return new static($container->get('plugin.manager.flashpoint_course_module_renderer'));
  }
}
