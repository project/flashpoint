<?php
/**
 * @file
 * Provides Drupal\flashpoint_course_module\FlashpointCourseModuleRendererInterface;
 */
namespace Drupal\flashpoint_course_module;
/**
 * An interface for all FlashpointCourseModuleRenderer type plugins.
 */
interface FlashpointCourseModuleRendererInterface {
  /**
   * Provide a description of the plugin.
   * @return string
   *   A string description of the plugin.
   */
  public function description();
}
