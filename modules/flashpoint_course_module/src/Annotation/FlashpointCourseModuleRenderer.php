<?php

namespace Drupal\flashpoint_course_module\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FlashpointCourseModuleRenderer annotation object.
 *
 * Plugin Namespace: Plugin\flashpoint_course_module
 *
 * @see plugin_api
 *
 * @Annotation
 */
class FlashpointCourseModuleRenderer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the FlashpointCourseModuleRenderer.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The category under which the FlashpointCourseModuleRenderer should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

}
