<?php


namespace Drupal\flashpoint_course_module\Breadcrumb;


use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;


class FlashpointCourseModuleBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $params = $route_match->getParameters()->all();
    if (isset($params['flashpoint_course_module']) && !empty($params['flashpoint_course_module'])) {
      return TRUE;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $url = Url::fromRoute('<front>');
    $breadcrumb->addLink(Link::fromTextAndUrl('Home', $url));

    /* @var $flashpoint_course_module \Drupal\flashpoint_course_module\Entity\FlashpointCourseModule */
    $flashpoint_course_module = \Drupal::routeMatch()->getParameter('flashpoint_course_module');

    $gcs = GroupContent::loadByEntity($flashpoint_course_module);
    if (!empty($gcs)) {
      $gc = array_shift($gcs);
      /* @var $gc \Drupal\group\Entity\GroupContent */
      $group = $gc->getGroup();
      $link = $group->toLink($group->label(), 'canonical');
      $breadcrumb->addLink($link);

      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
    else {
      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
  }
}