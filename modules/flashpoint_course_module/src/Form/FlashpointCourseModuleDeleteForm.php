<?php

namespace Drupal\flashpoint_course_module\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Course module entities.
 *
 * @ingroup flashpoint_course_module
 */
class FlashpointCourseModuleDeleteForm extends ContentEntityDeleteForm {


}
