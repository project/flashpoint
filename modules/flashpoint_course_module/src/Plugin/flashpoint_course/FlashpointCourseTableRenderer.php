<?php

namespace Drupal\flashpoint_course_module\Plugin\flashpoint_course;

use Drupal\Core\Plugin\PluginBase;
use Drupal\flashpoint_course\FlashpointCourseRendererInterface;
use Drupal\flashpoint_course\FlashpointCourseUtilities;
use Drupal\group\Entity\Group;
use Drupal\views\Views;

/**
 * @FlashpointCourseRenderer(
 *   id = "flashpoint_course_table_renderer",
 *   label = @Translation("Render content and modules as a searchable table"),
 * )
 */
class FlashpointCourseTableRenderer extends PluginBase implements FlashpointCourseRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Renders course using the default method.');
  }

  /**
   * @param $course
   * @return array
   */
  public static function renderCourse($course) {
    $course = Group::load($course);
    $description = $course->get('field_course_description')->getValue();
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');

    $form = [];

    if (FlashpointCourseUtilities::userIsEnrolled($course, \Drupal::currentUser()) || FlashpointCourseUtilities::isOpenAccessCourse($course)) {
      $args = [$course->id()];
      $module_view = Views::getView('flashpoint_course_course_content_table');
      $module_view->setArguments($args);
      $module_view->setDisplay('default');


      $form['course'] = $module_view->preview('default', $args);
    }

    return $form;
  }

}