<?php

namespace Drupal\flashpoint_course_module\Plugin\flashpoint_course_module;

use Drupal\Core\Plugin\PluginBase;
use Drupal\flashpoint_course_module\Entity\FlashpointCourseModule;
use Drupal\flashpoint_course_module\FlashpointCourseModuleRendererInterface;

/**
 * @FlashpointCourseModuleRenderer(
 *   id = "default",
 *   label = @Translation("Default Module Renderer: Description + Listing"),
 * )
 */
class DefaultModuleRenderer extends PluginBase implements FlashpointCourseModuleRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('This is a description of the default plugin.');
  }

  /**
   * @param FlashpointCourseModule $module
   * @return array
   */
  public static function renderModule(FlashpointCourseModule $module) {
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $instructional_content = $module->get('field_instructional_content')->getValue();
    $examination_content = $module->get('field_examination_content')->getValue();
    $add = [];
    if (!empty($instructional_content)) {
      $add['instructional_content'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $flashpoint_config->getOriginal('flashpoint_course_module.instructional_text'),
        '#items' => [],
        '#attributes' => ['class' => 'flashpoint-course-module-content instructional-content'],
      ];
      foreach ($instructional_content as $ic) {
        $item = \Drupal\flashpoint_course_content\Entity\FlashpointCourseContent::load($ic['target_id']);
        $add['instructional_content']['#items'][] = $item->renderListing();
      }
    }
    if (!empty($examination_content)) {
      $add['examination_content'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => $flashpoint_config->getOriginal('flashpoint_course_module.examination_text'),
        '#items' => [],
        '#attributes' => ['class' => 'flashpoint-course-module-content examination-content'],
      ];
      foreach ($examination_content as $ec) {
        $item = \Drupal\flashpoint_course_content\Entity\FlashpointCourseContent::load($ec['target_id']);
        $add['examination_content']['#items'][] = $item->renderListing();
      }
    }

    return $add;
  }
}
