<?php

namespace Drupal\flashpoint_course_module\Plugin\flashpoint_course_module;

use Drupal\Core\Plugin\PluginBase;
use Drupal\flashpoint_course_module\Entity\FlashpointCourseModule;
use Drupal\flashpoint_course_module\FlashpointCourseModuleRendererInterface;

/**
 * @FlashpointCourseModuleRenderer(
 *   id = "module_in_fieldset",
 *   label = @Translation("Module In Fieldset: Default Module Renderer wrapped in a fieldset"),
 * )
 */
class ModuleInFieldset extends PluginBase implements FlashpointCourseModuleRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('This is a description of the default plugin.');
  }

  /**
   * @param FlashpointCourseModule $module
   * @return array
   */
  public static function renderModule(FlashpointCourseModule $module) {
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $instructional_content = $module->get('field_instructional_content')->getValue();
    $examination_content = $module->get('field_examination_content')->getValue();
    $add = [];
    $add['module_' . $module->id() . '_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $module->label(),
      '#attributes' => ['class' => 'flashpoint-course-module-fieldset'],
    ];
    $add['module_' . $module->id() . '_fieldset']['content'] = DefaultModuleRenderer::renderModule($module);

    return $add;
  }
}
