<?php

namespace Drupal\flashpoint_course\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'flashpoint_course_renderer_type' field type.
 *
 * @FieldType(
 *   id = "flashpoint_course_renderer_type",
 *   label = @Translation("Flashpoint course renderer type"),
 *   category = @Translation("Flashpoint Education"),
 *   description = @Translation("Determines methods for enrolling in a course"),
 *   default_widget = "flashpoint_course_renderer_widget",
 *   default_formatter = "flashpoint_course_renderer_formatter"
 * )
 */
class FlashpointCourseRendererType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Text value'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  protected static function castAllowedValue($value) {
    return (string) $value;
  }

}
