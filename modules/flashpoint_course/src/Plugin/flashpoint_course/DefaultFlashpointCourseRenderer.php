<?php

namespace Drupal\flashpoint_course\Plugin\flashpoint_course;

use Drupal\Core\Plugin\PluginBase;
use Drupal\flashpoint_course\FlashpointCourseRendererInterface;
use Drupal\flashpoint_course\FlashpointCourseUtilities;
use Drupal\group\Entity\Group;

/**
 * @FlashpointCourseRenderer(
 *   id = "default_flashpoint_course_renderer",
 *   label = @Translation("Default: Render as vertical tabs, 1 tab per module."),
 * )
 */
class DefaultFlashpointCourseRenderer extends PluginBase implements FlashpointCourseRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Renders course using the default method.');
  }

  /**
   * @param $course
   * @return array
   */
  public static function renderCourse($course) {
    $course = Group::load($course);
    $description = $course->get('field_course_description')->getValue();
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');

    $form = [];

    // Course Preview.
    if (FlashpointCourseUtilities::previewAccess($course, \Drupal::currentUser())) {
      $form['course_container'] = [
        '#type' => 'vertical_tabs',
      ];
      if (isset($description[0])) {
        // Course Description.
        $form['course_description'] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => t('Course Description'),
          '#group' => 'course_container',
          // Put this at the top
          '#weight' => -99,
        ];
        $form['course_description']['description'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $description[0]['value'],
        ];
      }
      if (FlashpointCourseUtilities::viewAccess($course, \Drupal::currentUser())) {
        /*
         * Course Modules
         */
        $moduleHandler = \Drupal::service('module_handler');
        $add = [];
        if ($moduleHandler->moduleExists('flashpoint_course_module') && $moduleHandler->moduleExists('flashpoint_course_content')) {
          $add = DefaultFlashpointCourseRenderer::renderCourseWithModules($course, $flashpoint_config);
          if (empty($add)) {
            $add = DefaultFlashpointCourseRenderer::renderCourseWithoutModules($course, $flashpoint_config);
          }
        }
        /*
         * Course Content without modules
         */
        else if ($moduleHandler->moduleExists('flashpoint_course_content')) {
          $add = DefaultFlashpointCourseRenderer::renderCourseWithoutModules($course, $flashpoint_config);
        }
        foreach ($add as $key => $value) {
          $form[$key] = $value;
        }
      }
      // We don't have view access, explain next steps if we do not have enroll access.
      elseif (!FlashpointCourseUtilities::enrollAccess($course, \Drupal::currentUser())) {
        $form['course_description'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $description[0]['value'],
        ];
        $plugin_manager = \Drupal::service('plugin.manager.flashpoint_access');
        $plugin_definitions = $plugin_manager->getDefinitions();

        // View access
        $form['view_access_options'] = DefaultFlashpointCourseRenderer::renderViewOptions($course, $flashpoint_config, $plugin_definitions);

        // Enroll Access
        if (!FlashpointCourseUtilities::enrollAccess($course, \Drupal::currentUser())) {
          $form['enroll_access_options'] = DefaultFlashpointCourseRenderer::renderJoinOptions($course, $flashpoint_config, $plugin_definitions);
        }
      }
      // We do have enroll access, so no need for tabs:
      else {
        $form['course_description'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $description[0]['value'],
        ];
      }
    }

    return $form;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @return array
   */
  public static function renderCourseWithModules(Group $course, $flashpoint_config) {
    $args = [$course->id()];
    $add = [];

    $module_view = FlashpointCourseUtilities::getCourseModuleView($course);
    if (!empty($module_view->result)) {
      foreach ($module_view->result as $result) {
        $module = $result->_entity;
        $module = $module->getEntity();

        if ($module->isPublished()) {
          $add['module_' . $module->id()] = [
            '#type' => 'details',
            '#open' => TRUE,
            '#group' => 'course_container',
            '#title' => $module->label(),
            '#attributes' => ['class' => 'flashpoint-course-module-tab'],
          ];

          $plugin_manager = \Drupal::service('plugin.manager.flashpoint_course_module_renderer');
          $plugin_definitions = $plugin_manager->getDefinitions();
          $plugin_id = 'default';
          if (!empty($flashpoint_config->getOriginal('flashpoint_course_module.renderer'))) {
            $pid = $flashpoint_config->getOriginal('flashpoint_course_module.renderer');
            if (isset($plugin_definitions[$pid]['class'])) {
              $plugin_id = $pid;
            }
          }
          $module_rendered = $plugin_definitions[$plugin_id]['class']::renderModule($module);
          foreach ($module_rendered as $key => $value) {
            $add['module_' . $module->id()][$key] = $value;
          }

        }

      }
    }

    return $add;
  }

  /**
   * If no modules are present, or flashpoint_course_module is not enabled, run this.
   * @param Group $course
   * @param $flashpoint_config
   * @return array
   */
  public static function renderCourseWithoutModules(Group $course, $flashpoint_config) {
    $args = [$course->id()];
    $add = [];

    $content_view = FlashpointCourseUtilities::getCourseContentView($course);

    if (!empty($content_view->result)) {
      $add['course_content'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#group' => 'course_container',
        '#title' => t('Take Course'),
        '#attributes' => ['class' => 'flashpoint-course-content-tab'],
      ];
      $add['course_content']['content_list'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => t('Course Content'),
        '#items' => [],
        '#attributes' => ['class' => 'flashpoint-course-module-content course-content-no-module'],
      ];

      foreach ($content_view->result as $result) {
        $content = $result->_entity;
        // In some cases $result->_entity is what we wanted. If not, use this.
        if (method_exists($content, 'getEntity')) {
          $content = $content->getEntity();
        }
        $add['course_content']['content_list']['#items'][] = $content->renderListing();
      }
    }
    return $add;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @param $plugin_definitions
   * @return array
   */
  public static function renderViewOptions(Group $course, $flashpoint_config, $plugin_definitions) {
    $element = [];
    $view_plugins = $course->get('field_view_access')->getValue();
    $view_access_form_add = [];
    foreach ($view_plugins as $view_plugin) {
      $vp = $view_plugin['value'];
      $vp_array = $plugin_definitions[$vp]['class']::viewAccessFormElement($course, \Drupal::currentUser());
      $view_access_form_add = !empty($vp_array) ? array_merge($view_access_form_add, $vp_array) : $view_access_form_add;
    }
    if (!empty($view_access_form_add)) {
      $view_label = $flashpoint_config->getOriginal('flashpoint_course.view_access_label');
      $view_label = !empty($view_label) ? $view_label : t('Get Access');
      $element = [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $view_label,
        ]
      ];
      $element = array_merge($element, $view_access_form_add);
    }
    return $element;
  }

  /**
   * @param Group $course
   * @param $flashpoint_config
   * @param $plugin_definitions
   * @return array
   */
  public static function renderJoinOptions(Group $course, $flashpoint_config, $plugin_definitions) {
    $element = [];
    $enroll_plugins_and = $course->get('field_enrollment_conditions_and')->getValue();
    $enroll_plugins_or = $course->get('field_enrollment_conditions_or')->getValue();
    $enroll_label = $flashpoint_config->getOriginal('flashpoint_course.enroll_access_label');
    $enroll_label = !empty($enroll_label) ? $enroll_label : t('Enroll');
    $enroll_and_access_form_add = [];
    $enroll_or_access_form_add = [];


      foreach ($enroll_plugins_and as $enroll_plugin) {
        $ep = $enroll_plugin['value'];
        $ep_array = $plugin_definitions[$ep]['class']::joinAccessFormElement($course, \Drupal::currentUser());
        $enroll_and_access_form_add = !empty($ep_array) ? array_merge($enroll_and_access_form_add, $ep_array) : $enroll_and_access_form_add;
      }
      foreach ($enroll_plugins_or as $enroll_plugin) {
        $epor = $enroll_plugin['value'];
        $epor_array = $plugin_definitions[$epor]['class']::joinAccessFormElement($course, \Drupal::currentUser());
        $enroll_or_access_form_add = !empty($epor_array) ? array_merge($enroll_or_access_form_add, $epor_array) : $enroll_or_access_form_add;
      }
    if (\Drupal::currentUser()->isAnonymous() && (!empty($enroll_and_access_form_add) || !empty($enroll_or_access_form_add))) {
      $element = [
        '#type' => 'html_tag',
        '#weight' => 1,
        '#tag' => 'h3',
        '#value' => t('Please log in to see enrollment options'),
      ];
      $enroll_and_access_form_add = [];
      $enroll_or_access_form_add = [];
    }

    if (!empty($enroll_and_access_form_add) || !empty($enroll_or_access_form_add)) {
      $element = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $enroll_label,
      ];
      if (!empty($enroll_and_access_form_add)) {
        $element['enroll_access_and'] = [
          'header_and' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => '',
            'enroll_access_and_text' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('All of the following conditions must be met.'),
            ],
          ],
        ];
        $element['enroll_access_and'] = array_merge($element['enroll_access_and'], $enroll_and_access_form_add);
      }
      if (!empty($enroll_or_access_form_add)) {
        $element['enroll_access_or'] = [
          'header_or' => [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => '',
            'enroll_access_or_text' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('At least one of the following conditions must be met.'),
            ],
          ],
        ];
        $element['enroll_access_or'] = array_merge($element['enroll_access_or'], $enroll_or_access_form_add);
      }
    }
    return $element;
  }

}
