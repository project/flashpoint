<?php

namespace Drupal\flashpoint_course\Plugin\flashpoint_access;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "completed_prerequisite",
 *   label = @Translation("Completed Prerequisite Course"),
 *   group_type = "flashpoint_course"
 * )
 */
class CompletedPrerequisite extends PluginBase implements FlashpointAccessMethodInterface
{

  /**
   * @inheritdoc
   */
  public function description()
  {
    return $this->t('The user must have a pass record on a prerequisite course in order to enroll.');
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {
    // For now, we will just return TRUE, until the pass record logic is finished.
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = CompletedPrerequisite::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = CompletedPrerequisite::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    // Same logic as joinAccessFormElement()
    $element = CompletedPrerequisite::joinAccessFormElement($group, $account);
    return $element;
  }

  /**
   * @inheritdoc
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $element = [
      'completed_prerequisite' => [
        'header' => [
          '#type' => '#html_tag',
          '#tag' => 'p',
          '#value' => t('Completed Prerequisite: Complete the following prerequisite courses.'),
        ]
      ],
    ];
    // TODO add listing.
    return $element;
  }
}