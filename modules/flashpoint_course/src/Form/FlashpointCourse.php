<?php

namespace Drupal\flashpoint_course\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;

/**
 * Form controller for Flashpoint community content edit forms.
 *
 * @ingroup flashpoint_community_content
 */
class FlashpointCourse extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flashpoint_course';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $course = NULL) {
    if ($course) {
      $course = Group::load($course);
      $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
      $course_settings = $flashpoint_config->getOriginal('flashpoint_course');

      $plugin_manager = \Drupal::service('plugin.manager.flashpoint_course_renderer');
      $plugin_definitions = $plugin_manager->getDefinitions();
      // Use the default if this isn't set.
      $render_plugin = !empty($course_settings['renderer']) ? $course_settings['renderer'] : 'default_flashpoint_course_renderer';

      // If the course has a renderer set, use that instead.
      /* @var $course \Drupal\group\Entity\Group */
      if ($course->hasField('field_course_renderer')) {
        $course_renderer = $course->get('field_course_renderer')->getValue();
        if (isset($course_renderer[0]['value'])) {
          $render_plugin = $course_renderer[0]['value'];
        }
      }
      $form['flashpoint_course'] = $plugin_definitions[$render_plugin]['class']::renderCourse($course->id());

      // Set caching
      if (!isset($form['#cache']['contexts'])) {
        $form['#cache']['contexts'] = [];
      }
      $plugin_manager = \Drupal::service('plugin.manager.flashpoint_access');
      $plugin_definitions = $plugin_manager->getDefinitions();
      foreach ($plugin_definitions as $pd) {
        if (method_exists($pd['class'], 'addCacheContexts')) {
          $contexts = $pd['class']::addCacheContexts();
          foreach ($contexts as $context) {
            if (!in_array($context, $form['#cache']['contexts'])) {
              $form['#cache']['contexts'][] = $context;
            }
          }
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
