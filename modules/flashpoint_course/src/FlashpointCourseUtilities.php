<?php
/**
 * @file
 */

namespace Drupal\flashpoint_course;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;
use Drupal\views\Views;

/**
 * Class FlashpointCourseUtilities
 *
 * Provides some utilities functions commonly used in other modules.
 */
class FlashpointCourseUtilities {

  /**
   * @param string $group_type
   *
   * Group type may be "course" or "community"
   *
   * @return array $options
   */
  public static function getOptions($group_type = 'flashpoint_course') {

    $plugin_manager = \Drupal::service('plugin.manager.flashpoint_access');
    $plugin_definitions = $plugin_manager->getDefinitions();
    $options = [];
    foreach ($plugin_definitions as $pd) {
      if (!isset($pd['group_type']) || $pd['group_type'] === $group_type) {
        $options[$pd['id']] = $pd['label'];
      }
    }
    return $options;
  }

  /**
   * @return array $options
   */
  public static function getCourseRendererOptions() {
    $plugin_manager = \Drupal::service('plugin.manager.flashpoint_course_renderer');
    $plugin_definitions = $plugin_manager->getDefinitions();

    $options = [];
    foreach ($plugin_definitions as $pd) {
      $options[$pd['id']] = $pd['label'];
    }
    return $options;
  }

  /**
   * @return array $content
   */
  public static function getCourseContent(Group $course) {
    $moduleHandler = \Drupal::service('module_handler');

    if ($moduleHandler->moduleExists('flashpoint_course_content')) {
      $group_content = $course->getContent();
      foreach ($group_content as $gc) {
        /* @var $gc \Drupal\group\Entity\GroupContent */
        $gc->getEntityType();
      }
    }
    else {
      return [];
    }
  }

  /**
   * Get a View with a weighted list of course content.
   * @param Group $course
   * @return bool|\Drupal\views\ViewExecutable
   */
  public static function getCourseContentView(Group $course) {
    $moduleHandler = \Drupal::service('module_handler');

    if ($moduleHandler->moduleExists('flashpoint_course_content')) {
      $args = [$course->id()];
      $content_view = Views::getView('flashpoint_manage_course_content');
      $content_view->setArguments($args);
      $content_view->setDisplay('page_1');
      $content_view->preExecute();
      $content_view->execute();
      return $content_view;
    }
    else {
      return [];
    }
  }

  /**
   * Get a View with a weighted list of course modules.
   * @param Group $course
   * @return bool|\Drupal\views\ViewExecutable
   */
  public static function getCourseModuleView(Group $course) {
    $moduleHandler = \Drupal::service('module_handler');

    if ($moduleHandler->moduleExists('flashpoint_course_module')) {
      $args = [$course->id()];
      $module_view = Views::getView('flashpoint_manage_course_modules_in_course');
      $module_view->setArguments($args);
      $module_view->setDisplay('page_1');
      $module_view->preExecute();
      $module_view->execute();
      return $module_view;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param $group
   * @param null $account
   * @return bool
   *   Whether the account is a enrolled in the course.
   */
  public static function userIsEnrolled($group, $account = NULL) {
    if (is_numeric($group)) {
      $group = Group::load($group);
    }
    $account = $account ? $account : \Drupal::currentUser();
    return $group->getMember($account) ? TRUE : FALSE;
  }

  /**
   * @param GroupInterface $group
   * @return bool
   */
  public static function isOpenAccessCourse(GroupInterface $group) {
    if ($group->hasField('field_open_access_course')) {
      $open = $group->get('field_open_access_course')->getValue();
      if (isset($open[0]['value']) && $open[0]['value']) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @param GroupInterface $group
   * @return bool
   */
  public static function trackCourseProgress(GroupInterface $group) {
    $track = $group->get('field_track_course_progress')->getValue();
    if (isset($track[0]['value']) && $track[0]['value']) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Determines whether someone is able to enroll in a course.
   *
   * Returns TRUE if they have access, FALSE if they do not.
   * Also returns FALSE if the user is already enrolled.
   *
   * @param GroupInterface $course
   * @param AccountInterface $account
   * @param string $return_type
   * @return array|bool
   */
  public static function enrollAccess(GroupInterface $course, AccountInterface $account) {
    // Is the person already enrolled?
    if (FlashpointCourseUtilities::userIsEnrolled($course, $account)) {
      return FALSE;
    }
    // Next, are they able to join in a group context?
    $accessManager = \Drupal::service('access_manager');
    if(!$course->hasPermission('join group', $account)) {
      return FALSE;
    }
    // Check for "join" access
    $enroll_methods = FlashpointCourseUtilities::courseAccess($course, $account, 'checkAccess', ['field_enrollment_conditions_and', 'field_enrollment_conditions_or']);
    if((!in_array(FALSE, $enroll_methods['field_enrollment_conditions_and'])
        || empty($enroll_methods['field_enrollment_conditions_and']))
      && (in_array(TRUE, $enroll_methods['field_enrollment_conditions_or'])
        || empty($enroll_methods['field_enrollment_conditions_or']))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param GroupInterface $group
   * @param AccountInterface $account
   * @return bool
   */
  public static function viewAccess(GroupInterface $group, AccountInterface $account) {
    $enroll_methods = FlashpointCourseUtilities::courseAccess($group, $account, 'checkViewAccess', ['field_view_access']);

    /*
     * Logic of return statement.
     *  - Open access courses get view access (deprecated).
     *  - Enrolled users have view access.
     *  - If all view access plugins return TRUE (not FALSE), grant access.
     *  - If there are no view access plugins and other conditions are false...
     *     - View access overriding membership is not allowed, access is denied.
     */
    return FlashpointCourseUtilities::userIsEnrolled($group, $account) ||
      FlashpointCourseUtilities::isOpenAccessCourse($group) ||
      (!empty($enroll_methods['field_view_access']) &&
        !in_array(FALSE, $enroll_methods['field_view_access']));
  }

  /**
   * @param GroupInterface $group
   * @param AccountInterface $account
   * @return bool
   */
  public static function previewAccess(GroupInterface $group, AccountInterface $account) {
    $enroll_methods = FlashpointCourseUtilities::courseAccess($group, $account, 'checkPreviewAccess', ['field_preview_access']);

    /*
     * Logic of return statement.
     *  - Open access courses get preview access (deprecated).
     *  - Enrolled users have preview access.
     *  - View access grants preview access.
     *  - If all preview access plugins return TRUE (not FALSE), grant access.
     *  - If there are no preview access plugins and other conditions are false...
     *     - Preview is not prevented, and access is granted.
     */

    return FlashpointCourseUtilities::userIsEnrolled($group, $account) ||
      FlashpointCourseUtilities::viewAccess($group, $account) ||
      FlashpointCourseUtilities::isOpenAccessCourse($group) ||
      !in_array(FALSE, $enroll_methods['field_preview_access']) ||
      empty($enroll_methods['field_preview_access']);
  }

  /**
   * Course access logic
   * @param GroupInterface $course
   * @param AccountInterface $account
   * @param string $access_function
   * @param array $access_fields
   * @return array|bool
   */
  public static function courseAccess(GroupInterface $course, AccountInterface $account, $access_function = 'checkAccess', $access_fields = ['field_enrollment_conditions_and', 'field_enrollment_conditions_or']) {
    // First check the person isn't a member already.
    if(!$course->getMember($account)) {
      $enroll_methods = [];
      $eval_plugins = [];
      $plugin_manager = \Drupal::service('plugin.manager.flashpoint_access');
      $plugin_definitions = $plugin_manager->getDefinitions();
      foreach ($access_fields as $enroll_type) {
        $enroll_methods[$enroll_type] = [];
        foreach ($course->get($enroll_type)->getValue() as $posn => $arr) {
          // Filter invalid entries
          if (in_array($arr['value'], array_keys($plugin_definitions))) {
            $enroll_methods[$enroll_type][$arr['value']] = FALSE;
            if (!in_array($arr['value'], $eval_plugins)) {
              $eval_plugins[] = $arr['value'];
            }
          }
        }
      }
      foreach ($eval_plugins as $ep) {
        if(method_exists($plugin_definitions[$ep]['class'], 'checkAccess')) {
          $access = $plugin_definitions[$ep]['class']::{$access_function}($course, $account);
          foreach ($enroll_methods as $enroll_type => $list) {
            if(key_exists($ep, $list)) {
              $enroll_methods[$enroll_type][$ep] = $access;
            }
          }
        }
      }
      return $enroll_methods;
    }
    // Either the method is 'access', or something invalid
    return FALSE;
  }

  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return array
   *   A group join form.
   */
  public function groupJoinForm(GroupInterface $group) {
    $join_text = t('Join group');
    switch ($group->bundle()) {
      case 'flashpoint_course':
        $join_text = t('Enroll');
        break;
      default:
        $join_text = t('Join group');
        break;
    }
    /** @var \Drupal\group\Plugin\GroupContentEnablerInterface $plugin */
    $plugin = $group->getGroupType()->getContentPlugin('group_membership');

    // Pre-populate a group membership with the current user.
    $group_content = GroupContent::create([
      'type' => $plugin->getContentTypeConfigId(),
      'gid' => $group->id(),
      'entity_id' => \Drupal::currentUser()->id(),
    ]);

    $form = \Drupal::service('entity.form_builder')->getForm($group_content, 'group-join');
    $form['actions']['submit']['#value'] = $join_text;
    return $form;
  }
}
