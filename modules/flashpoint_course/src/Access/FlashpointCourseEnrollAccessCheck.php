<?php

namespace Drupal\flashpoint_course\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;
use Drupal\flashpoint_course\FlashpointCourseUtilities;

/**
 * Class FlashpointCourseEnrollAccessCheck
 * @package Drupal\flashpoint_course\Access
 */
class FlashpointCourseEnrollAccessCheck implements AccessInterface {

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $course = $route_match->getParameter('group');
    if ($course && $course->get('type')->getValue()[0]['target_id'] === 'flashpoint_course') {
      return AccessResult::allowedIf(FlashpointCourseUtilities::enrollAccess($course, $account));
    }
    return AccessResult::allowed();
  }
}