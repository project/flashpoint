<?php

namespace Drupal\Tests\flashpoint_course\Unit\Config;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\UnitTestCase;

/**
 *
 * @group flashpoint
 */
class FlashpointCourseConfigTest extends UnitTestCase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

  }

  /**
   * Confirm configs do not contain references to modules which may not be enabled.
   */
  public function testConfirmNoConfigConflicts() {
    
    $this->assertTrue(TRUE);
  }

}
