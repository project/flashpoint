<?php

namespace Drupal\flashpoint_course_community_bridge\Plugin\flashpoint_access;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "community_member",
 *   label = @Translation("Member of a selected community")
 * )
 */
class CommunityMember extends PluginBase implements FlashpointAccessMethodInterface {

  /**
   * @inheritdoc
   */
  public function description() {
    return $this->t('The user must be a member of a specified community.');
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {

    $allowed_ids = [];
    if ($group->hasField('field_eligible_communities')) {
      foreach ($group->get('field_eligible_communities')->getValue() as $item) {
        $allowed_ids[] = $item['target_id'];
      }
    }

    $memberships = \Drupal::service('group.membership_loader')->loadByUser($account);

    foreach ($memberships as $membership) {
      $group_type = $membership->getGroup()->bundle();
      if ($group_type === 'flashpoint_community' && in_array($membership->getGroup()->id(), $allowed_ids)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = CommunityMember::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = CommunityMember::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    return [];
  }

  /**
   * @inheritdoc
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $element = [
      'community_member' => [
        '#type' => '#html_tag',
        '#tag' => 'p',
        '#value' => t('Community Member: Join an eligible community to gain access.'),
      ],
    ];
    return $element;
  }

}