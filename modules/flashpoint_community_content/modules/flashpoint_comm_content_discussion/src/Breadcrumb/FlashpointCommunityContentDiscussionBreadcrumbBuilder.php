<?php


namespace Drupal\flashpoint_comm_content_discussion\Breadcrumb;


use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;


class FlashpointCommunityContentDiscussionBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $params = $route_match->getParameters()->all();
    if (isset($params['flashpoint_community_content']) && !empty($params['flashpoint_community_content'])) {
      /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
      $flashpoint_community_content = $params['flashpoint_community_content'];

      /*
       * This breadcrumb applies to discussion_post and discussion_category content,
       */
      if (in_array($flashpoint_community_content->bundle(), ['discussion_post', 'discussion_category'])) {
        return TRUE;
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $url = Url::fromRoute('<front>');
    $breadcrumb->addLink(Link::fromTextAndUrl('Home', $url));

    /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
    $flashpoint_community_content = \Drupal::routeMatch()->getParameter('flashpoint_community_content');

    $gcs = GroupContent::loadByEntity($flashpoint_community_content);
    if (!empty($gcs)) {
      $gc = array_shift($gcs);
      /* @var $gc \Drupal\group\Entity\GroupContent */
      $group = $gc->getGroup();
      $link = $group->toLink($group->label(), 'canonical');
      $breadcrumb->addLink($link);

      // Discussion items have different routes depending on the bundle.
      switch ($flashpoint_community_content->bundle()) {
        case 'discussion_post':
          $d_url = Url::fromRoute('view.flashpoint_community_discussion.page_3', ['group' => $group->id()]);
          $d_link = Link::fromTextAndUrl('Discussion', $d_url);
          $breadcrumb->addLink($d_link);
          break;
        case 'discussion_category':
          $d_url = Url::fromRoute('view.flashpoint_community_discussion.manage_categories', ['group' => $group->id()]);
          $d_link = Link::fromTextAndUrl('Discussion', $d_url);
          $breadcrumb->addLink($d_link);
          break;
      }

      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
    else {
      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
  }
}