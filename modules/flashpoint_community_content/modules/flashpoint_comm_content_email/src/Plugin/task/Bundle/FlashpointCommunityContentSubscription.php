<?php

namespace Drupal\flashpoint_comm_content_email\Plugin\task\Bundle;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\flashpoint_comm_content_email\FlashpointCommunityContentEmailUtilities;
use Drupal\group\Entity\Group;
use Drupal\task\Entity\Task;
use Drupal\task\Entity\TaskInterface;
use Drupal\task\TaskBundleInterface;
use Drupal\user\Entity\User;

/**
 * @TaskBundle(
 *   id = "flashpoint_comm_content_sub",
 *   label = @Translation("Flashpoint Community Content Subscription"),
 *   bundle = "flashpoint_comm_content_sub"
 * )
 */
class FlashpointCommunityContentSubscription extends PluginBase implements TaskBundleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Subscription manager for Flashpoint Community Content');
  }

  /**
   * @param $task_data
   */
  public static function createTask($task_data) {
    // Look for a pre-existing subscription, and add to it
    $query = \Drupal::entityQuery('task');
    $query->condition('assigned_by', $task_data['assigned_by']);
    $query->condition('assigned_by_type', $task_data['assigned_by_type']);
    $query->condition('assigned_to', $task_data['assigned_to']);
    $query->condition('assigned_to_type', $task_data['assigned_to_type']);
    $result = $query->execute();
    if (empty($result)) {
      $task = Task::create($task_data);
      $task->save();
    }
  }

  /**
   * @param $task_data
   */
  public static function expireTask($task) {
    $data = $task->get('task_data')->getValue();
    if (isset($data[0]['actions']['expire'])) {
      $action_data = $data[0]['actions']['expire'];
      foreach ($action_data as $id => $data) {
        $plugin_manager = \Drupal::service('plugin.manager.task_action');
        $plugin_definitions = $plugin_manager->getDefinitions();
        if(isset($plugin_definitions[$id])) {
          $plugin_definitions[$id]['class']::doAction($task, $data);
        }
      }
    }
    $task->set('status', 'closed');
    $task->set('close_date', time());
    $task->set('close_type', 'closed');
    $task->save();
    return $task;
  }

  /**
   * @param TaskInterface $task
   * @return array
   */
  public static function getTaskOptions(TaskInterface $task) {
    // The url is example code in case we need it in the future.
//    $url_complete = Url::fromRoute('task.mark_complete', ['task' => $task->id()]);
//    $link_complete = Link::fromTextAndUrl('Mark Complete', $url_complete);

    return ['#type' => 'markup', '#markup' => ''];
  }

}
