<?php

namespace Drupal\flashpoint_comm_content_email\Plugin\task\Bundle;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\flashpoint_comm_content_email\FlashpointCommunityContentEmailUtilities;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\task\Entity\Task;
use Drupal\task\Entity\TaskInterface;
use Drupal\task\TaskBundleInterface;
use Drupal\task\Plugin\task\Action\SendMail;
use Drupal\user\Entity\User;

/**
 * @TaskBundle(
 *   id = "flashpoint_cc_send_queue",
 *   label = @Translation("Flashpoint Send Queue"),
 *   bundle = "flashpoint_cc_send_queue"
 * )
 */
class FlashpointCommunityContentSendQueue extends PluginBase implements TaskBundleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Subscription manager for Flashpoint Community Content');
  }

  /**
   * @param $task_data
   */
  public static function createTask($task_data) {
    $task_data['task_data']['query_range_start'] = '0';
    $task_data['expire_date'] = time();
    $task = Task::create($task_data);
    $task->save();
    FlashpointCommunityContentSendQueue::expireTask($task);
  }

  /**
   * @param $task
   * @return mixed
   */
  public static function expireTask($task) {
    $data = $task->get('task_data')->getValue();
    $data = array_shift($data);
    // TODO Add an option to set the step size
    $stepsize = 1;

    if (isset($data['actions']['expire'])) {
      $action_data = $data['actions']['expire'];
      foreach ($action_data as $id => $data) {
        $plugin_manager = \Drupal::service('plugin.manager.task_action');
        $plugin_definitions = $plugin_manager->getDefinitions();
        if(isset($plugin_definitions[$id])) {
          $plugin_definitions[$id]['class']::doAction($task, $data);
        }
      }
    }
    $task_data  = ['task_data' => $data];
    foreach (['assigned_by', 'assigned_by_type', 'assigned_to', 'assigned_to_type'] as $key) {
      if (!empty($task->get($key)->getValue())) {
        $task_data[$key] = $task->get($key)->getValue()[0]['value'];
      }
    }
    /*
     * If "assigned_to_type" is not "user," then we have eligible
     * content that needs to sent to the subscribed list.
     *
     * If it is not "discussion_content," and it is new, then we
     * should alert everyone.
     *
     * If it is "user," we are sending a user's emails.
     */
    if ($task_data['assigned_to_type'] !== 'user' && $task_data['task_data']['operation'] === 'update') {
      $result = TRUE;
      while ($result) {
        // Look for subscriptions other than the user who started the activity.
        $query = \Drupal::entityQuery('task');
        $query->condition('type', 'flashpoint_comm_content_sub');
        $query->condition('assigned_by', $task_data['assigned_by'], '<>');
        $query->condition('assigned_by_type', 'user');
        $query->condition('assigned_to', $task_data['assigned_to']);
        $query->condition('assigned_to_type', $task_data['assigned_to_type']);
        $query->range($task_data['task_data']['query_range_start'],$stepsize);
        $query->sort('id', 'DESC');
        $result = $query->execute();
        if ($result) {
          $tasks = Task::loadMultiple($result);
          foreach ($tasks as $t) {

            // Get the user's email queue, and check preferences
            $uid = $t->get('assigned_by')->getValue()[0]['value'];
            $g = Group::load($task_data['task_data']['group']);
            $u = User::load($uid);
            $frequency = FlashpointCommunityContentEmailUtilities::getUpdateFrequency($g, $u, 'string');
            if ($frequency) {

              $tquery = \Drupal::entityQuery('task');
              $tquery->condition('type', 'flashpoint_cc_send_queue');
              $tquery->condition('assigned_to', $uid);
              $tquery->condition('assigned_to_type', 'user');
              $tresult = $tquery->execute();

              // If the queue exists, set it to active and add the data
              if (!empty($tresult)) {
                $user_queue = Task::load(array_shift($tresult));
                $user_queue_data = array_shift($user_queue->get('task_data')->getValue());

                if ($user_queue->getStatus() === 'closed') {
                  $frequency = FlashpointCommunityContentEmailUtilities::getUpdateFrequency($g, $u);
                  // Calculate next send time.
                  $close_date = array_shift($user_queue->get('close_date')->getValue());
                  $close_date = $close_date['value'];
                  if (($close_date + $frequency) < time()) {
                    $user_queue->set('expire_date', time());
                  }
                  else {
                    $user_queue->set('expire_date', ($close_date + $frequency));
                  }
                  // Set to active
                  $user_queue->set('status', 'active');
                  $user_queue->set('close_date', 0);
                  $user_queue_data['activity'] = [
                    [
                      'operation' => 'update',
                      'comment' => $task_data['task_data']['comment'],
                      'assigned_to' => $task_data['assigned_to'],
                      'assigned_to_type' => $task_data['assigned_to_type'],
                      'assigned_by' => $task_data['assigned_by'],
                      'assigned_by_type' => $task_data['assigned_by_type'],
                    ],
                  ];
                  $user_queue->set('task_data', $user_queue_data);
                  $user_queue->save();
                }
                // Queue is active. Add to it.
                else {
                  $activity = [
                    'operation' => 'update',
                    'comment' => $task_data['task_data']['comment'],
                    'assigned_to' => $task_data['assigned_to'],
                    'assigned_to_type' => $task_data['assigned_to_type'],
                    'assigned_by' => $task_data['assigned_by'],
                    'assigned_by_type' => $task_data['assigned_by_type'],
                  ];
                  // Prevents duplicates
                  if (!in_array($activity, $user_queue_data['activity'])) {
                    $user_queue_data['activity'][] = $activity;
                    $user_queue->set('task_data', $user_queue_data);
                    $user_queue->save();
                  }
                }
              }
              /*
               * There is no queue. Create one.
               * We checked earlier that we have a frequency.
               */
              else {
                $uq = [
                  'type' => 'flashpoint_cc_send_queue',
                  'assigned_to' => $uid,
                  'assigned_to_type' => 'user',
                  'status' => 'active',
                  'expire_date' => time(),
                  'task_data' => [
                    'activity' => [
                      [
                        'operation' => 'update',
                        'comment' => $task_data['task_data']['comment'],
                        'assigned_to' => $task_data['assigned_to'],
                        'assigned_to_type' => $task_data['assigned_to_type'],
                        'assigned_by' => $task_data['assigned_by'],
                        'assigned_by_type' => $task_data['assigned_by_type'],
                      ],
                    ],
                  ],
                ];
                $uqtask = Task::create($uq);
                $uqtask->save();
                FlashpointCommunityContentSendQueue::expireTask($uqtask);
              }
            }
          }
          $task_data['task_data']['query_range_start'] += $stepsize;
          $task->set('task_data', $data)->save();
        }
        else {
          $task->set('status', 'closed');
          $task->set('close_date', time());
          $task->set('close_type', 'flashpoint_cc_email_scheduled');
          $task->save();
          return $task;
        }
      }
    }
    // If we are creating new content, alert all applicable users
    if ($task_data['assigned_to_type'] !== 'discussion_content'
      && $task_data['assigned_to_type'] !== 'user'
      && $task_data['task_data']['operation'] === 'insert') {
      // Get all memberships for the community
      $result = TRUE;
      while ($result) {
        $query = \Drupal::entityQuery('group_content');
        $query->condition('type', 'group_content_type_bb71eeef6aa80');
        $query->condition('gid', $task_data['task_data']['group']);
        $query->condition('entity_id', $task_data['assigned_by'], '<>');
        $query->range($task_data['task_data']['query_range_start'], $stepsize);
        $query->sort('id', 'DESC');
        $result = $query->execute();
        foreach ($result as $r) {
          $gc = GroupContent::load($r);
          // Get the user's email queue
          $uid = $gc->get('entity_id')->getValue()[0]['target_id'];
          $freq = FlashpointCommunityContentEmailUtilities::getUpdateFrequency($gc->getGroup(), User::load($uid), 'string');
          if ($freq !== 'none') {
            $tquery = \Drupal::entityQuery('task');
            $tquery->condition('type', 'flashpoint_cc_send_queue');
            $tquery->condition('assigned_to', $uid);
            $tquery->condition('assigned_to_type', 'user');
            $tresult = $tquery->execute();
            // If the queue exists, set it to active and add the data
            if (!empty($tresult)) {
              $user_queue = Task::load(array_shift($tresult));
              $user_queue_data = array_shift($user_queue->get('task_data')->getValue());

              if ($user_queue->getStatus() === 'closed') {
                // Get frequency and calculate next send time.
                $frequency = FlashpointCommunityContentEmailUtilities::getUpdateFrequency($gc->getGroup(), User::load($uid));
                $close_date = array_shift($user_queue->get('close_date')->getValue());
                $close_date = $close_date['value'];
                if (($close_date + $frequency) < time()) {
                  $user_queue->set('expire_date', time());
                }
                else {
                  $user_queue->set('expire_date', ($close_date + $frequency));
                }
                // Set to active
                $user_queue->set('status', 'active');
                $user_queue->set('close_date', 0);
                $user_queue_data['activity'] = [
                  [
                    'operation' => 'insert',
                    'comment' => $task_data['task_data']['comment'],
                    'assigned_to' => $task_data['assigned_to'],
                    'assigned_to_type' => $task_data['assigned_to_type'],
                    'assigned_by' => $task_data['assigned_by'],
                    'assigned_by_type' => $task_data['assigned_by_type'],
                  ],
                ];
                $user_queue->set('task_data', $user_queue_data);
                $user_queue->save();
              }
              // Queue is active. Add to it.
              else {
                $user_queue_data['activity'][] = [
                  'operation' => 'insert',
                  'comment' => $task_data['task_data']['comment'],
                  'assigned_to' => $task_data['assigned_to'],
                  'assigned_to_type' => $task_data['assigned_to_type'],
                  'assigned_by' => $task_data['assigned_by'],
                  'assigned_by_type' => $task_data['assigned_by_type'],
                ];
                $user_queue->set('task_data', $user_queue_data);
                $user_queue->save();
              }
            }
            // If the queue does not exist, create it.
            else {
              $uq = [
                'type' => 'flashpoint_cc_send_queue',
                'assigned_to' => $uid,
                'assigned_to_type' => 'user',
                'status' => 'active',
                'expire_date' => time(),
                'task_data' => [
                  'activity' => [
                    [
                      'operation' => 'insert',
                      'comment' => $task_data['task_data']['comment'],
                      'assigned_to' => $task_data['assigned_to'],
                      'assigned_to_type' => $task_data['assigned_to_type'],
                      'assigned_by' => $task_data['assigned_by'],
                      'assigned_by_type' => $task_data['assigned_by_type'],
                    ],
                  ],
                ],
              ];
              $uqtask = Task::create($uq);
              $uqtask->save();
              FlashpointCommunityContentSendQueue::expireTask($uqtask);
            }
          }
        }
        $task_data['task_data']['query_range_start'] += $stepsize;
        $task->set('task_data', $data)->save();
      }
      // We have finished sending messages, close this task.
      $task->set('status', 'closed');
      $task->set('close_date', time());
      $task->set('close_type', 'flashpoint_cc_email_scheduled');
      $task->save();
    }
    // If we are looking at a user's "send queue," send out the messages
    if ($task_data['assigned_to_type'] === 'user') {
      $tdata = $task->get('task_data')->getValue();
      $u = User::load($task_data['assigned_to']);
      if (!empty($tdata)) {
        $tdata = array_shift($tdata);
        if (isset($tdata['activity']) && !empty($tdata['activity'])) {
          $subject = t('New Activity on ') . \Drupal::config('system.site')->get('name');
          $message = '<p>' . t('Hello ') . $u->label() . ',</p>';
          $message .= '<p>' . t('You have new content waiting for you on ') . \Drupal::config('system.site')->get('name') . ':</p>';
          foreach ($tdata['activity'] as $activity) {
            $by = \Drupal::entityTypeManager()->getStorage($activity['assigned_by_type'])->load($activity['assigned_by']);
            // Note, "content" is always community content.
            $content = \Drupal::entityTypeManager()->getStorage('flashpoint_community_content')->load($activity['assigned_to']);
            $content_url = Url::fromRoute('entity.flashpoint_community_content.canonical', ['flashpoint_community_content' => $activity['assigned_to']]);
            $content_link = Link::fromTextAndUrl($content->label(), $content_url)->toString();
            if ($activity['comment']) {
              $message .= '<p>' . $by->label() . t(' commented on ') . $content_link . '</p>';
            }
            elseif ($activity['operation'] === 'insert') {
              $message .= '<p>' . $by->label() . t(' posted ') . $content_link . '</p>';
            }
            elseif ($activity['operation'] === 'update') {
              $message .= '<p>' . $by->label() . t(' updated ') . $content_link . '</p>';
            }


          }
          $params = [
              'from' => \Drupal::config('system.site')->get('mail'),
              'subject' => $subject,
              'message' => [$message],
            ];
          $to = $u->getEmail();
          $module = 'task';
          $key = 'task_mail';
          $langcode = \Drupal::currentUser()->getPreferredLangcode();
          $send = TRUE;
          $reply = $params['from'];
          $mailManager = \Drupal::service('plugin.manager.mail');
          $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
          $task->set('status', 'closed');
          $task->set('close_date', time());
          $task->set('close_type', 'flashpoint_cc_email_sent');
          $task->save();
        }
      }
    }
  }

  /**
   * @param TaskInterface $task
   * @return array
   */
  public static function getTaskOptions(TaskInterface $task) {
    // The url is example code in case we need it in the future.
//    $url_complete = Url::fromRoute('task.mark_complete', ['task' => $task->id()]);
//    $link_complete = Link::fromTextAndUrl('Mark Complete', $url_complete);

    return ['#type' => 'markup', '#markup' => ''];
  }

}
