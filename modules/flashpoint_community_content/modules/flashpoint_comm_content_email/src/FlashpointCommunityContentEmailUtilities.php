<?php

namespace Drupal\flashpoint_comm_content_email;

use Drupal\group\Entity\Group;
use Drupal\task\TaskUtilities;
use Drupal\user\Entity\User;

/**
 * Event subscriber for entity insert and update events.
 */
class FlashpointCommunityContentEmailUtilities {

  public static function getUpdateFrequency(Group $group, User $user, $type = 'integer') {
    $membership = $group->getMember($user)->getGroupContent();

    // Get the frequency string
    $frequency = 'none';
    $freq = $membership->get('field_email_preferences')->getValue();
    if (isset($freq[0]['value'])) {
      $frequency = $freq[0]['value'];
    }
    else {
      $freq = $user->get('field_email_preference_community')->getValue();
      if (isset($freq[0]['value'])) {
        $frequency = $freq[0]['value'];
      }
    }

    if ($type === 'string') {
      return $frequency;
    }
    elseif ($type === 'integer') {
      // Calculate the frequency duration from the string
      switch($frequency) {
        case 'daily':
          return 86400;
          break;
        case 'immediate':
          return 1;
          break;
        case 'none':
        default:
          return 0;
          break;
      }
    }
  }

}
