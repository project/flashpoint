<?php

namespace Drupal\flashpoint_comm_content_email\EventSubscriber;

use Drupal\Core\Url;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\entity_events\EventSubscriber\EntityEventUpdateSubscriber;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\task\TaskUtilities;
use Drupal\user\Entity\User;

/**
 * Event subscriber for entity insert and update events.
 */
class FlashpointCommunityContentEmailUpdateEventSubscriber extends EntityEventUpdateSubscriber {

  public function onEntityUpdate(EntityEvent $event) {
    $op = 'update';
    $entity = $event->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    if($entity_type === 'group_content'
      && $entity->getGroup()->bundle() === 'flashpoint_community'
      && $bundle !== 'group_content_type_bb71eeef6aa80') {
      $ent = $entity->getEntity();
      $type = $ent->bundle();
      $owner_id = $ent->getOwner()->id();
      $task_data = [
        'type' => 'flashpoint_comm_content_sub',
        'assigned_to' => $ent->id(),
        'assigned_to_type' => $type,
        'task_data' => [
          'operation' => $op,
          'comment' => FALSE,
          'group' => $entity->getGroup()->id(),
        ],
      ];
      $task_data['assigned_by'] = $owner_id;
      $task_data['assigned_by_type'] = 'user';
      TaskUtilities::createTask($task_data);
      $task_data['type'] = 'flashpoint_cc_send_queue';
      TaskUtilities::createTask($task_data);
    }
    elseif ($entity_type === 'comment' && $bundle === "flashpoint_community_comments") {
      $ent = $entity->getCommentedEntity();
      $gc = GroupContent::loadByEntity($ent);
      $gc = array_shift($gc);
      $type = $ent->bundle();
      $id = $ent->id();
      $owner_id = $entity->getOwner()->id();
      $task_data = [
        'type' => 'flashpoint_comm_content_sub',
        'assigned_to' => $id,
        'assigned_to_type' => $type,
        'task_data' => [
          'operation' => $op,
          'comment' => TRUE,
          'group' => $gc->getGroup()->id(),
        ],
      ];
      $task_data['assigned_by'] = $owner_id;
      $task_data['assigned_by_type'] = 'user';
      TaskUtilities::createTask($task_data);
      $task_data['type'] = 'flashpoint_cc_send_queue';
      TaskUtilities::createTask($task_data);
    }
  }

}
