<?php


namespace Drupal\flashpoint_community_content\Breadcrumb;


use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;


class FlashpointCommunityContentBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $params = $route_match->getParameters()->all();
    if (isset($params['flashpoint_community_content']) && !empty($params['flashpoint_community_content'])) {
      /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
      $flashpoint_community_content = $params['flashpoint_community_content'];

      /*
       * This breadcrumb applies to post content only,
       * as it is the only bundle shipping with the base module.
       * Other modules should add their own breadcrumb logic for their own bundle.
       */
      if ($flashpoint_community_content->bundle() === 'post') {
        return TRUE;
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $url = Url::fromRoute('<front>');
    $breadcrumb->addLink(Link::fromTextAndUrl('Home', $url));

    /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
    $flashpoint_community_content = \Drupal::routeMatch()->getParameter('flashpoint_community_content');

    $gcs = GroupContent::loadByEntity($flashpoint_community_content);
    if (!empty($gcs)) {
      $gc = array_shift($gcs);
      /* @var $gc \Drupal\group\Entity\GroupContent */
      $group = $gc->getGroup();
      $link = $group->toLink($group->label(), 'canonical');
      $breadcrumb->addLink($link);

      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
    else {
      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
  }
}