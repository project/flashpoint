<?php

namespace Drupal\flashpoint_community_content;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for flashpoint_community_content.
 */
class FlashpointCommunityContentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
