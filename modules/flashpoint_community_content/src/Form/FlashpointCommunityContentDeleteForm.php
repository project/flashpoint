<?php

namespace Drupal\flashpoint_community_content\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Flashpoint community content entities.
 *
 * @ingroup flashpoint_community_content
 */
class FlashpointCommunityContentDeleteForm extends ContentEntityDeleteForm {


}
