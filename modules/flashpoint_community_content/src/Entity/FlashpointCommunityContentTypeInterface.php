<?php

namespace Drupal\flashpoint_community_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Flashpoint community content type entities.
 */
interface FlashpointCommunityContentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
