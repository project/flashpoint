<?php

namespace Drupal\flashpoint_community\Plugin\flashpoint_access;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "any_community_member",
 *   label = @Translation("Member of any community")
 * )
 */
class AnyCommunityMember extends PluginBase implements FlashpointAccessMethodInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('The user must be a member of any community.');
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {
    $memberships = \Drupal::service('group.membership_loader')->loadByUser($account);
    foreach ($memberships as $membership) {
      $group_type = $membership->getGroup()->bundle();
      if ($group_type === 'flashpoint_community') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = AnyCommunityMember::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkAccess()
    $access = AnyCommunityMember::checkAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    // Same logic as joinAccessFormElement()
    $element = AnyCommunityMember::joinAccessFormElement($group, $account);
    return $element;
  }

  /**
   * @inheritdoc
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $element = [
      'any_community_member' => [
        '#type' => '#html_tag',
        '#tag' => 'p',
        '#value' => t('Join any community on our site to gain access.'),
      ],
    ];
    return $element;
  }

}
