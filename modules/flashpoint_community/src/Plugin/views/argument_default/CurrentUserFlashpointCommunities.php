<?php

namespace Drupal\flashpoint_community\Plugin\views\argument_default;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * Default argument plugin to extract the current user's groups.
 *
 * This plugin actually has no options so it does not need to do a great deal.
 *
 * @ViewsArgumentDefault(
 *   id = "current_user_flashpoint_communities",
 *   title = @Translation("The Flashpoint Communites of the current user.")
 * )
 */
class CurrentUserFlashpointCommunities extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $u = \Drupal::currentUser()->getAccount();
    $ms = \Drupal::service('group.membership_loader')->loadByUser($u);
    $ret = [];
    foreach ($ms as $m) {
      $type = $m->getGroupContent()->get('type')->getValue()[0]['target_id'];
      // 'group_content_type_bb71eeef6aa80' is the type for community membership.
      if($type === 'group_content_type_bb71eeef6aa80') {
        $ret[] = $m->getGroup()->id();
      }
    }
    return implode('+',$ret);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // TODO add cache tags.
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

}
