<?php

/**
 * @file
 * Contains flashpoint_access_code.page.inc.
 *
 * Page callback for Flashpoint Access Code entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Flashpoint Access Code templates.
 *
 * Default template: flashpoint_access_code.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_flashpoint_access_code(array &$variables) {
  // Fetch FlashpointAccessCode Entity Object.
  $flashpoint_access_code = $variables['elements']['#flashpoint_access_code'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
