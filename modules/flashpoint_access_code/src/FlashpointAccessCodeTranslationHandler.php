<?php

namespace Drupal\flashpoint_access_code;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for flashpoint_access_code.
 */
class FlashpointAccessCodeTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
