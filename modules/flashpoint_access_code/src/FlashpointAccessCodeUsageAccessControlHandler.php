<?php

namespace Drupal\flashpoint_access_code;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Flashpoint access code usage entity.
 *
 * @see \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsage.
 */
class FlashpointAccessCodeUsageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished flashpoint access code usage entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published flashpoint access code usage entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit flashpoint access code usage entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete flashpoint access code usage entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add flashpoint access code usage entities');
  }

}
