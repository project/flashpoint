<?php

namespace Drupal\flashpoint_access_code\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsage;
use Drupal\flashpoint_access_code\FlashpointAccessCodeUtilities;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Class FlashpointAccessCodeSettingsForm.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeRedemptionForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'flashpoint_access_code_redemption';
  }

  /**
   * Defines the settings form for Flashpoint Access Code entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['access_code'] = ['#type' => 'textfield'];
    $form['access_code']['#title'] = 'Please enter the access code you have received for this course.';
    $group = \Drupal::routeMatch()->getParameter('group');
    $form['group_id'] = ['#type' => 'hidden', '#value' => $group->id()];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit'
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @throws EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $code = $form_state->getValue('access_code');
    $group_id = $form_state->getValue('group_id');
    $redeemer = User::load(\Drupal::currentUser()->id());
    /* @var $redeemer User */
    $query = \Drupal::entityQuery('flashpoint_access_code')
      ->condition('access_code', $code)
      ->condition('group_id', $group_id);
    $result = $query->execute();
    $messenger = \Drupal::messenger();
    // If the access code doesn't exist, do not redeem
    if (!count($result)) {
      $messenger->addError(t('The access code <strong><em>%s</em></strong> was not found for this course.', ['%s' => $code]));
    }
    // Check that the person may use the code.
    else {
      $acid = array_shift($result);
      $access_code = FlashpointAccessCode::load($acid);
      /* @var $access_code FlashpointAccessCode */

      $type = $access_code->get('field_redemption_type')->value;
      $type = !empty($type) ? $type : 'join';

      switch ($type) {
        case 'join':
          $this->redeemJoinAccessCode($form_state, $group_id, $redeemer, $access_code);
          break;
        case 'view':
          $this->redeemViewAccessCode($form_state, $group_id, $redeemer, $access_code);
          break;
      }
    }
  }

  /**
   * @param FormStateInterface $form_state
   * @param $group_id
   * @param User $redeemer
   * @param FlashpointAccessCode $access_code
   * @throws EntityStorageException
   */
  private function redeemJoinAccessCode(FormStateInterface $form_state, $group_id, User $redeemer, FlashpointAccessCode $access_code) {
    $code = $form_state->getValue('access_code');
    FlashpointAccessCodeUtilities::redeemJoinAccessCode($code, $group_id, $redeemer, $access_code);
    $form_state->setRedirect('entity.group.canonical', ['group' => $group_id]);
  }

  /**
   * @param FormStateInterface $form_state
   * @param $group_id
   * @param User $redeemer
   * @param FlashpointAccessCode $access_code
   * @throws EntityStorageException
   */
  private function redeemViewAccessCode(FormStateInterface $form_state, $group_id, User $redeemer, FlashpointAccessCode $access_code) {
    $code = $form_state->getValue('access_code');
    FlashpointAccessCodeUtilities::redeemViewAccessCode($code, $group_id, $redeemer, $access_code);
    $form_state->setRedirect('entity.group.canonical', ['group' => $group_id]);
  }

}
