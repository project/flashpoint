<?php

namespace Drupal\flashpoint_access_code\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Flashpoint Access Code entities.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeDeleteForm extends ContentEntityDeleteForm {


}
