<?php

namespace Drupal\flashpoint_access_code\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;

/**
 * Form controller for Flashpoint Access Code edit forms.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\flashpoint_access_code\Entity\FlashpointAccessCode */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;
    $query = \Drupal::request()->query;
    if($query->has('group_id')) {
      $gid = $query->get('group_id');
      if (is_numeric($gid)) {
        $group = Group::load($gid);
        if (!empty($group)) {
          $form['group_id']['widget'][0]['target_id']['#default_value'] = $group;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Flashpoint Access Code.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Flashpoint Access Code.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.flashpoint_access_code.canonical', ['flashpoint_access_code' => $entity->id()]);
  }

}
