<?php

namespace Drupal\flashpoint_access_code\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Flashpoint access code usage entities.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeUsageDeleteForm extends ContentEntityDeleteForm {


}
