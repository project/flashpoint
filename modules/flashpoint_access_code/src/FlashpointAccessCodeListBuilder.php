<?php

namespace Drupal\flashpoint_access_code;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\group\Entity\Group;

/**
 * Defines a class to build a listing of Flashpoint Access Code entities.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Flashpoint Access Code ID');
    $header['name'] = $this->t('Name');
    $header['code'] = $this->t('Code');
    $header['group'] = $this->t('Course/Community');
    $header['type'] = $this->t('Redemption Type');
    $header['max_usage'] = $this->t('Max Usage');
    $header['current usage'] = $this->t('Current Usage');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\flashpoint_access_code\Entity\FlashpointAccessCode */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.flashpoint_access_code.edit_form',
      ['flashpoint_access_code' => $entity->id()]
    );
    $row['code'] = $entity->get('access_code')->value;
    $group_id = $entity->get('group_id')->getValue();
    if (isset($group_id[0]['target_id'])) {
      $group = Group::load($group_id[0]['target_id']);
      $row['group'] = $group->label();
    }
    else {
      $row['group'] = $this->t('none');
    }
    $type = $entity-> get('field_redemption_type')->value;
    $row['type'] = $type ? $type : 'join (default)';
    $row['max_usage'] = $entity->getMaxUsage();
    $row['current usage'] = $entity->getCurrentUsage();
    return $row + parent::buildRow($entity);
  }

}
