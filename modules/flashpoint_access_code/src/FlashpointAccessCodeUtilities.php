<?php

namespace Drupal\flashpoint_access_code;

use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsage;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Class FlashpointAccessCodeUtilities
 * @package Drupal\flashpoint_access_code
 */
class FlashpointAccessCodeUtilities {

  /**
   * @param $code
   * @param $group_id
   * @param User $redeemer
   * @param FlashpointAccessCode $access_code
   * @param bool $success_message
   * @throws EntityStorageException
   */
  public static function redeemJoinAccessCode($code, $group_id, User $redeemer, FlashpointAccessCode $access_code, $success_message = TRUE) {
    $messenger = \Drupal::messenger();
    $grant_access = FALSE;
    $previous = FALSE;

    $max = $access_code->getMaxUsage();
    $current = $access_code->getCurrentUsage();

    if (!$max || $current < $max) {
      $grant_access = TRUE;
    }
    else {
      $messenger->addError(t('The access code <strong><em>%s</em></strong> has been used the maximum number of times, and cannot be used for additional redemptions.', ['%s' => $code]));
    }

    // Double check that the user hasn't used the code already.
    if ($grant_access) {
      $previous = $access_code->getPreviousRedemption($redeemer->id());
      if ($previous) {
        $handler = \Drupal::config('flashpoint.settings')->get('flashpoint_access_code.previous_redemption');
        switch($handler) {
          case 'grant':
            $grant_access = TRUE;
            break;
          case 'grant_increment':
            $grant_access = TRUE;
            $previous = FALSE;
            break;
          case 'deny':
          default:
            $messenger->addError(t('You have used the access code <strong><em>%s</em></strong> previously, and may not re-use it.', ['%s' => $code]));
            $grant_access = FALSE;
            break;
        }
      }
    }

    // If access is granted, then proceed with creating a membership.
    if ($grant_access) {
      $group = Group::load($group_id);
      $membership = $group->getMember($redeemer);
      if (!$membership && !$redeemer->isAnonymous()) {
        $group->addMember($redeemer);
        if (!$previous) {
          $usage_values = [
            'user_id' => $redeemer->id(),
            'redeemer' => $redeemer->id(),
            'redeemer_name' => $redeemer->label(),
            'redeemer_type' => 'user',
            'group_id' => $group_id,
            'access_code' => $access_code->id(),
            'status' => TRUE,
          ];
          $usage = FlashpointAccessCodeUsage::create($usage_values);
          $usage->save();
        }
        if ($success_message) {
          $messenger->addStatus(t('You have redeemed the access code <strong><em>%s</em></strong>.', ['%s' => $code]));
        }
      }
      elseif ($membership) {
        $messenger->addError('You already have access to this course. If you are having trouble with access, please contact the site administrator.');
      }
      elseif ($redeemer->isAnonymous()) {
        $messenger->addError('You have attempted to redeem an enrollment access code, but are not logged in. Please log in or create an account to redeem this access code.');
      }
    }
  }

  /**
   * @param $code
   * @param $group_id
   * @param User $redeemer
   * @param FlashpointAccessCode $access_code
   * @param bool $success_message
   * @throws EntityStorageException
   */
  public static function redeemViewAccessCode($code, $group_id, User $redeemer, FlashpointAccessCode $access_code, $success_message = TRUE) {
    $messenger = \Drupal::messenger();

    // If the person is not a member (already has access), grant view access.
    $group = Group::load($group_id);
    $membership = $group->getMember($redeemer);
    if (!$membership) {
      $c = \Drupal::request()->cookies->all();
      if (empty($c['Drupal_visitor_flashpoint_access_code'])) {
        user_cookie_save(['flashpoint_access_code' => $code]);
      }
      else {
        $code_concat = implode('|', [$c['Drupal_visitor_flashpoint_access_code'], $code]);
        user_cookie_save(['flashpoint_access_code'=> $code_concat]);
      }
      $usage_values = [
        'user_id' => $redeemer->id(),
        'redeemer' => $redeemer->id(),
        'redeemer_name' => $redeemer->isAnonymous() ? \Drupal::request()->getClientIp() : $redeemer->label(),
        'redeemer_type' => $redeemer->isAnonymous() ? 'ip' : 'user',
        'group_id' => $group_id,
        'access_code' => $access_code->id(),
        'status' => TRUE,
      ];
      $usage = FlashpointAccessCodeUsage::create($usage_values);
      $usage->save();

      if ($success_message) {
        $messenger->addStatus(t('You have redeemed the access code <strong><em>%s</em></strong>.', ['%s' => $code]));
      }
    }
    else {
      $messenger->addError('You already have access to this course. If you are having trouble with access, please contact the site administrator.');
    }
  }

}
