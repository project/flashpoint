<?php

namespace Drupal\flashpoint_access_code;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;
use Drupal\group\Entity\Group;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of Flashpoint access code usage entities.
 *
 * @ingroup flashpoint_access_code
 */
class FlashpointAccessCodeUsageListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Flashpoint access code usage ID');
    $header['redeemer_name'] = $this->t('Redeemer Name/IP');
    $header['redeemer_type'] = $this->t('Redeemer Type');
    $header['group_id'] = $this->t('Course');
    $header['code'] = $this->t('Code');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsage */
    $row['id'] = $entity->id();
    $redeemer = User::load($entity->redeemer->target_id);
    $row['redeemer_name'] = $redeemer->id() ? $redeemer->toLink($redeemer->label()) : $entity->redeemer_name->value;
    $row['redeemer_type'] = $entity->redeemer_type->value;
    $group = Group::load($entity->group_id->target_id);
    $row['group_id'] = $group->toLink($group->label());
    $code = FlashpointAccessCode::load($entity->access_code->target_id);
    $row['code'] = $code->toLink($code->label());
    return $row + parent::buildRow($entity);
  }

}
