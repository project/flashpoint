<?php

namespace Drupal\flashpoint_access_code\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;
use Drupal\flashpoint_access_code\FlashpointAccessCodeUtilities;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class FlashpointAccessCodeController
 * @package Drupal\flashpoint_access_code\Controller
 */
class FlashpointAccessCodeController extends ControllerBase {

  /**
   * @param GroupInterface $group
   * @param $code
   * @return RedirectResponse
   * @throws EntityStorageException
   */
  public function redeemAuto(GroupInterface $group, $code) {
    $redeemer = User::load(\Drupal::currentUser()->id());
    /* @var $redeemer User */
    $group_id = $group->id();
    $query = \Drupal::entityQuery('flashpoint_access_code')
      ->condition('access_code', $code)
      ->condition('group_id', $group_id);
    $result = $query->execute();
    $messenger = \Drupal::messenger();
    // If the access code doesn't exist, do not redeem
    if (!count($result)) {
      $messenger->addError(t('The access code <strong><em>%s</em></strong> was not found for this course.', ['%s' => $code]));
    }
    // Check that the person may use the code.
    else {
      $acid = array_shift($result);
      $access_code = FlashpointAccessCode::load($acid);
      /* @var $access_code FlashpointAccessCode */

      $type = $access_code->get('field_redemption_type')->value;
      $type = !empty($type) ? $type : 'join';

      switch ($type) {
        case 'join':
          FlashpointAccessCodeUtilities::redeemJoinAccessCode($code, $group_id, $redeemer, $access_code, FALSE);
          break;
        case 'view':
          FlashpointAccessCodeUtilities::redeemViewAccessCode($code, $group_id, $redeemer, $access_code, FALSE);
          break;
      }
    }
    $url = Url::fromRoute('entity.group.canonical', ['group' => $group_id]);
    $response = new RedirectResponse($url->toString());
    return $response;
  }

}
