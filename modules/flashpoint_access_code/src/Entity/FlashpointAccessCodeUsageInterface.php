<?php

namespace Drupal\flashpoint_access_code\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Flashpoint access code usage entities.
 *
 * @ingroup flashpoint_access_code
 */
interface FlashpointAccessCodeUsageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Flashpoint access code usage name.
   *
   * @return string
   *   Name of the Flashpoint access code usage.
   */
  public function getName();

  /**
   * Sets the Flashpoint access code usage name.
   *
   * @param string $name
   *   The Flashpoint access code usage name.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsageInterface
   *   The called Flashpoint access code usage entity.
   */
  public function setName($name);

  /**
   * Gets the Flashpoint access code usage creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Flashpoint access code usage.
   */
  public function getCreatedTime();

  /**
   * Sets the Flashpoint access code usage creation timestamp.
   *
   * @param int $timestamp
   *   The Flashpoint access code usage creation timestamp.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsageInterface
   *   The called Flashpoint access code usage entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Flashpoint access code usage published status indicator.
   *
   * Unpublished Flashpoint access code usage are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Flashpoint access code usage is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Flashpoint access code usage.
   *
   * @param bool $published
   *   TRUE to set this Flashpoint access code usage to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsageInterface
   *   The called Flashpoint access code usage entity.
   */
  public function setPublished($published);

}
