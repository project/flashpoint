<?php

namespace Drupal\flashpoint_access_code\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Flashpoint access code usage entities.
 */
class FlashpointAccessCodeUsageViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
