<?php

namespace Drupal\flashpoint_access_code\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Flashpoint access code usage entity.
 *
 * @ingroup flashpoint_access_code
 *
 * @ContentEntityType(
 *   id = "flashpoint_access_code_usage",
 *   label = @Translation("Flashpoint access code usage"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flashpoint_access_code\FlashpointAccessCodeUsageListBuilder",
 *     "views_data" = "Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeUsageViewsData",
 *     "translation" = "Drupal\flashpoint_access_code\FlashpointAccessCodeUsageTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeUsageForm",
 *       "add" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeUsageForm",
 *       "edit" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeUsageForm",
 *       "delete" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeUsageDeleteForm",
 *     },
 *     "access" = "Drupal\flashpoint_access_code\FlashpointAccessCodeUsageAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\flashpoint_access_code\FlashpointAccessCodeUsageHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "flashpoint_access_code_usage",
 *   data_table = "flashpoint_access_code_usage_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer flashpoint access code usage entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "redeemer_name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/flashpoint/flashpoint_access_code_usage/{flashpoint_access_code_usage}",
 *     "add-form" = "/admin/structure/flashpoint/flashpoint_access_code_usage/add",
 *     "edit-form" = "/admin/structure/flashpoint/flashpoint_access_code_usage/{flashpoint_access_code_usage}/edit",
 *     "delete-form" = "/admin/structure/flashpoint/flashpoint_access_code_usage/{flashpoint_access_code_usage}/delete",
 *     "collection" = "/admin/structure/flashpoint/flashpoint_access_code_usage",
 *   },
 *   field_ui_base_route = "flashpoint_access_code_usage.settings"
 * )
 */
class FlashpointAccessCodeUsage extends ContentEntityBase implements FlashpointAccessCodeUsageInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Flashpoint access code usage entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['redeemer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Access Code Redeemer'))
      ->setDescription(t('The user ID of the user who is redeeming the code.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['redeemer_name'] = \Drupal\Core\Field\BaseFieldDefinition::create('string')
      ->setLabel(t('Redeemer'))
      ->setDescription(t('The User who redeemed the access code.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
    $fields['redeemer_type'] = \Drupal\Core\Field\BaseFieldDefinition::create('string')
      ->setLabel(t('Redeemer Type'))
      ->setDescription(t('Should be "user" or "id".'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['group_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setDescription(t('The group the user was given access to.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'group')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['access_code'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Access Code'))
      ->setDescription(t('The access code that was used to gain access.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'flashpoint_access_code')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Flashpoint access code usage is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
