<?php

namespace Drupal\flashpoint_access_code\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Flashpoint Access Code entity.
 *
 * @ingroup flashpoint_access_code
 *
 * @ContentEntityType(
 *   id = "flashpoint_access_code",
 *   label = @Translation("Flashpoint Access Code"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flashpoint_access_code\FlashpointAccessCodeListBuilder",
 *     "views_data" = "Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeViewsData",
 *     "translation" = "Drupal\flashpoint_access_code\FlashpointAccessCodeTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeForm",
 *       "add" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeForm",
 *       "edit" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeForm",
 *       "delete" = "Drupal\flashpoint_access_code\Form\FlashpointAccessCodeDeleteForm",
 *     },
 *     "access" = "Drupal\flashpoint_access_code\FlashpointAccessCodeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\flashpoint_access_code\FlashpointAccessCodeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "flashpoint_access_code",
 *   data_table = "flashpoint_access_code_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer flashpoint access code entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/flashpoint_access_code/{flashpoint_access_code}",
 *     "add-form" = "/flashpoint_access_code/add",
 *     "edit-form" = "/flashpoint_access_code/{flashpoint_access_code}/edit",
 *     "delete-form" = "/flashpoint_access_code/{flashpoint_access_code}/delete",
 *     "collection" = "/admin/structure/flashpoint/flashpoint_access_code",
 *   },
 *   field_ui_base_route = "flashpoint_access_code.settings"
 * )
 */
class FlashpointAccessCode extends ContentEntityBase implements FlashpointAccessCodeInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxUsage() {
    return $this->get('maximum_usage')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentUsage() {
    $query = \Drupal::entityQuery('flashpoint_access_code_usage');
    $query->condition('access_code', $this->id());
    $result = $query->execute();
    return count($result);
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousRedemption($user_id) {
    $query = \Drupal::entityQuery('flashpoint_access_code_usage');
    $query->condition('access_code', $this->id());
    $query->condition('redeemer', $user_id);
    $result = $query->execute();
    return count($result);
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Flashpoint Access Code entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['group_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Group to grant access'))
      ->setDescription(t('The Group this access code will grant access to.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'group')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Administrative Name for Access Code'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['access_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Access Code'))
      ->setDescription(t('The Access Code'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->addConstraint('flashpoint_unique_access_code');

    $fields['maximum_usage'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Maximum Usage'))
      ->setDescription(t('The maximum number of times this code may be used. Set to 0 for unlimited use.'))
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Flashpoint Access Code is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
