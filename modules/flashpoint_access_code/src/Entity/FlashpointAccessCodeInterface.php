<?php

namespace Drupal\flashpoint_access_code\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Flashpoint Access Code entities.
 *
 * @ingroup flashpoint_access_code
 */
interface FlashpointAccessCodeInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Flashpoint Access Code name.
   *
   * @return string
   *   Name of the Flashpoint Access Code.
   */
  public function getName();

  /**
   * Sets the Flashpoint Access Code name.
   *
   * @param string $name
   *   The Flashpoint Access Code name.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeInterface
   *   The called Flashpoint Access Code entity.
   */
  public function setName($name);

  /**
   * Gets the Flashpoint Access Code creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Flashpoint Access Code.
   */
  public function getCreatedTime();

  /**
   * Sets the Flashpoint Access Code creation timestamp.
   *
   * @param int $timestamp
   *   The Flashpoint Access Code creation timestamp.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeInterface
   *   The called Flashpoint Access Code entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Flashpoint Access Code published status indicator.
   *
   * Unpublished Flashpoint Access Code are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Flashpoint Access Code is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Flashpoint Access Code.
   *
   * @param bool $published
   *   TRUE to set this Flashpoint Access Code to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\flashpoint_access_code\Entity\FlashpointAccessCodeInterface
   *   The called Flashpoint Access Code entity.
   */
  public function setPublished($published);

}
