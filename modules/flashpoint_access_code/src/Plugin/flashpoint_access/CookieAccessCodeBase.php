<?php

namespace Drupal\flashpoint_access_code\Plugin\flashpoint_access;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * Class CookieAccessCodeBase
 * @package Drupal\flashpoint_access_code\Plugin\flashpoint_access
 */
class CookieAccessCodeBase extends PluginBase implements FlashpointAccessMethodInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Has a cookie with a valid access code');
  }

  public static function addCacheContexts() {
    return ['cookies:Drupal_visitor_flashpoint_access_code'];
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {
    $cookie = \Drupal::request()->cookies->get('Drupal_visitor_flashpoint_access_code');
    if(!empty($cookie)) {
      $group_id = $group->id();
      $strs = explode('|', $cookie);
      foreach ($strs as $str) {
        $query = \Drupal::entityQuery('flashpoint_access_code')
          ->condition('access_code', $str)
          ->condition('group_id', $group_id)
          ->condition('field_redemption_type', 'join')
          ->condition('status', TRUE);
        $result = $query->execute();
        if (!empty($result)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Same logic as checkViewAccess()
    $view_access = CookieAccessCodeBase::checkViewAccess($group, $account);
    $join_access = CookieAccessCodeBase::checkAccess($group, $account);
    return $view_access || $join_access;
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    $cookie = \Drupal::request()->cookies->get('Drupal_visitor_flashpoint_access_code');
    if(!empty($cookie)) {
      $group_id = $group->id();
      $strs = explode('|', $cookie);
      foreach ($strs as $str) {
        $query = \Drupal::entityQuery('flashpoint_access_code')
          ->condition('access_code', $str)
          ->condition('group_id', $group_id)
          ->condition('field_redemption_type', 'view')
          ->condition('status', TRUE);
        $result = $query->execute();
        if (!empty($result)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $attrs = ['attributes' => ['class' => [$flashpoint_config->getOriginal('flashpoint_access_code.view_button_class')]]];
    $url = Url::fromRoute('flashpoint_access_code.redeem', ['group' => $group->id()], $attrs);
    $link = Link::fromTextAndUrl('Redeem Access Code', $url);
    $element = [
      'cookie_access_code' => [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $flashpoint_config->getOriginal('flashpoint_access_code.view_button_text'),
        ],
        'link' => $link->toRenderable(),
      ],
    ];
    return $element;
  }

  /**
   * @inheritdoc
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $attrs = ['attributes' => ['class' => [$flashpoint_config->getOriginal('flashpoint_access_code.enroll_button_class')]]];
    $url = Url::fromRoute('flashpoint_access_code.redeem', ['group' => $group->id()], $attrs);
    $link = Link::fromTextAndUrl('Redeem Access Code', $url);
    $element = [
      'cookie_access_code' => [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $flashpoint_config->getOriginal('flashpoint_access_code.enroll_button_text'),
        ],
        'link' => $link->toRenderable(),
      ],
    ];
    return $element;
  }

}
