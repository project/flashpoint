<?php

namespace Drupal\flashpoint_access_code\Plugin\flashpoint_access;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "cookie_access_code_no_form",
 *   label = @Translation("Flashpoint Access Code: Hide redemption form")
 * )
 */
class CookieAccessCodeNoForm extends CookieAccessCodeBase {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Flashpoint Access Code: Hide redemption form');
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    $element = [];
    return $element;
  }

  /**
   * @inheritdoc
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $element = [];
    return $element;
  }

}
