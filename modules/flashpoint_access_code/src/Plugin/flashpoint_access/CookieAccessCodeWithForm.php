<?php

namespace Drupal\flashpoint_access_code\Plugin\flashpoint_access;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "cookie_access_code",
 *   label = @Translation("Flashpoint Access Code: Show redemption form")
 * )
 */
class CookieAccessCodeWithForm extends CookieAccessCodeBase {

}
