<?php


namespace Drupal\flashpoint_access_code\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Defines a views field plugin.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsField("access_code_usage_count")
 */

class FlashpointAccessCodeUsageCount extends FieldPluginBase {
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }
  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $data) {
    $ac = $this->getEntity($data);
    $current = $ac->getCurrentUsage();
    return [
      '#type' => 'markup',
      '#markup' => $current,
    ];
  }
}
