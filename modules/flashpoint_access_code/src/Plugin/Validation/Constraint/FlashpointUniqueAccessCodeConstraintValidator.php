<?php

namespace Drupal\flashpoint_access_code\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\group\Entity\Group;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;

/**
 * Class FlashpointUniqueAccessCodeConstraintValidator
 * Validates the flashpoint_unique_access_code constraint.
 * Looks for a matching code + group, and returns an error if found.
 *
 * @package Drupal\flashpoint_access_code\Plugin\Validation\Constraint
 */
class FlashpointUniqueAccessCodeConstraintValidator extends ConstraintValidator
{
  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $item = $items->first();
    if (isset($item->getValue()['value'])) {
      $val = $item->getValue()['value'];

      $query = \Drupal::entityQuery('flashpoint_access_code');
      $query->condition('access_code', $val);

      $ent = $items->getEntity();
      $group_id = $ent->get('group_id')->getValue();
      if (isset($group_id[0]['target_id'])) {
        $query->condition('group_id', $group_id[0]['target_id']);
      }

      $result = $query->execute();

      if (!empty($result)) {
        $c = count($result);
        $r = array_shift($result);
        if ($c > 1 || $r !== $ent->id()) {
          $this->context->addViolation($constraint->duplicateCode);
        }
      }
    }
  }
}