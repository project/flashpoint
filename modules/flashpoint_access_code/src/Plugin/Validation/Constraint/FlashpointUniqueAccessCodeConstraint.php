<?php

namespace Drupal\flashpoint_access_code\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted duration is of the format HH:MM:SS
 *
 * @Constraint(
 *   id = "flashpoint_unique_access_code",
 *   label = @Translation("Unique Access Code", context = "Validation"),
 * )
 */
class FlashpointUniqueAccessCodeConstraint extends Constraint {

  public $duplicateCode = 'The given access code already exists for the selected course. Please choose a different code, or apply this code to a different course.';

}