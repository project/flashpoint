<?php

namespace Drupal\flashpoint_access_code\Plugin\flashpoint_settings;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointSettingsInterface;


/**
 * @FlashpointSettings(
 *   id = "access_code_options",
 *   label = @Translation("Course Render Options"),
 * )
 */
class AccessCodeOptions extends PluginBase implements FlashpointSettingsInterface
{
  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Settings for the default course content renderer.');
  }

  /**
   * Provide form options for the settings form.
   * @return array
   *   Array of Form API form elements.
   */
  public static function getFormOptions() {
    $flashpoint_config = \Drupal::configFactory()->getEditable('flashpoint.settings');
    $ret = [
      'access_code_options' => [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => t('Access Code Options'),
        '#group' => 'flashpoint',
        'flashpoint_access_code__previous_redemption' => [
          '#type' => 'select',
          '#title' => t('Previous Redemption Handling'),
          '#description' => t('The action to take if a user is redeeming a code they had already redeemed previously.'),
          '#empty_option' => t(' - Select - '),
          '#default_value' => $flashpoint_config->getOriginal('flashpoint_access_code.previous_redemption'),
          '#options' => [
            'grant_increment' => t('Grant access to the course again, and count it as an additional usage of the access code.'),
            'grant' => t('Grant access to the course again, and re-use the original redemption.'),
            'deny' => t('Deny access to the course if the user does not have access.'),
          ],
        ],
        'flashpoint_access_code__view_button_text' => [
          '#type' => 'textfield',
          '#title' => t('View Access Button Header Text'),
          '#description' => t('The header text over a View Access redemption button.'),
          '#default_value' => $flashpoint_config->getOriginal('flashpoint_access_code.view_button_text'),
        ],
        'flashpoint_access_code__view_button_class' => [
          '#type' => 'textfield',
          '#title' => t('View Access Button Classes'),
          '#description' => t('A string containing CSS classes for decorating View Access Buttons.'),
          '#default_value' => $flashpoint_config->getOriginal('flashpoint_access_code.view_button_class'),
        ],
        'flashpoint_access_code__enroll_button_text' => [
          '#type' => 'textfield',
          '#title' => t('Enroll Access Button Header Text'),
          '#description' => t('The header text over a Enroll Access redemption button.'),
          '#default_value' => $flashpoint_config->getOriginal('flashpoint_access_code.enroll_button_text'),
        ],
        'flashpoint_access_code__enroll_button_class' => [
          '#type' => 'textfield',
          '#title' => t('Enroll Access Button Classes'),
          '#description' => t('A string containing CSS classes for decorating Enroll Access Buttons.'),
          '#default_value' => $flashpoint_config->getOriginal('flashpoint_access_code.enroll_button_class'),
        ],
      ],
    ];
    return $ret;
  }

}
