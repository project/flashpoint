<?php

namespace Drupal\flashpoint_access_code\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\flashpoint_access_code\Entity\FlashpointAccessCode;
use Drupal\group\Entity\Group;
use Drupal\webform\Element\WebformMessage;
use Drupal\webform\Plugin\WebformElement\WebformManagedFileBase;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformMessageManagerInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission remote post handler.
 *
 * @WebformHandler(
 *   id = "flashpoint_create_access_code",
 *   label = @Translation("Flashpoint Education: Create Access Code"),
 *   category = @Translation("Flashpoint Education"),
 *   description = @Translation("Creates an access code using webform submission"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class CreateAccessCode extends WebformHandlerBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The webform message manager.
   *
   * @var \Drupal\webform\WebformMessageManagerInterface
   */
  protected $messageManager;

  /**
   * A webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * List of unsupported webform submission properties.
   *
   * The below properties will not being included in a remote post.
   *
   * @var array
   */
  protected $unsupportedProperties = [
    'metatag',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, WebformSubmissionConditionsValidatorInterface $conditions_validator, ModuleHandlerInterface $module_handler, ClientInterface $http_client, WebformTokenManagerInterface $token_manager, WebformMessageManagerInterface $message_manager, WebformElementManagerInterface $element_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_factory, $config_factory, $entity_type_manager, $conditions_validator);
    $this->moduleHandler = $module_handler;
    $this->httpClient = $http_client;
    $this->tokenManager = $token_manager;
    $this->messageManager = $message_manager;
    $this->elementManager = $element_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('webform_submission.conditions_validator'),
      $container->get('module_handler'),
      $container->get('http_client'),
      $container->get('webform.token_manager'),
      $container->get('webform.message_manager'),
      $container->get('plugin.manager.webform.element')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $field_names = array_keys(\Drupal::service('entity_field.manager')->getBaseFieldDefinitions('webform_submission'));
    $excluded_data = array_combine($field_names, $field_names);
    return [
      'name' => '',
      'access_code' => '',
      'field_redemption_type' => 'view',
      'maximum_usage' => 0,
      'group_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    $this->configuration['group_id'] = $this->configuration['group_id'] ? Group::load($this->configuration['group_id']) : NULL;
    $form['create'] = [
      '#type' => 'details',
      '#title' => t('Access Code'),
      '#open' => true,
    ];
    $form['create']['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name of Access Code'),
      '#required' => TRUE,
      '#description' => t('The administrative name of the access code'),
      '#default_value' => $this->configuration['name'],
    ];
    $form['create']['access_code'] = [
      '#type' => 'textfield',
      '#title' => t('Access Code'),
      '#required' => TRUE,
      '#description' => t('The Access Code, which can be redeemed.'),
      '#default_value' => $this->configuration['access_code'],
    ];
    $form['create']['field_redemption_type'] = [
      '#type' => 'select',
      '#title' => t('Redemption Type'),
      '#required' => TRUE,
      '#options' => [
        'view' => t('View Access'),
        'join' => t('Join/Enroll Access'),
      ],
      '#default_value' => $this->configuration['field_redemption_type'],
    ];
    $form['create']['maximum_usage'] = [
      '#type' => 'number',
      '#title' => t('Maximum Usage'),
      '#required' => TRUE,
      '#description' => t('The maximum number of redemptions, or 0 for unlimited.'),
      '#default_value' => $this->configuration['maximum_usage'],
    ];
    $form['create']['group_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => t('Grant access to this group'),
      '#required' => TRUE,
      '#target_type' => 'group',
      '#default_value' => $this->configuration['group_id'],
    ];

    // Submission data.
    $form['submission_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission data'),
    ];
    // Display warning about file uploads.
    if ($this->getWebform()->hasManagedFile()) {
      $form['submission_data']['managed_file_message'] = [
        '#type' => 'webform_message',
        '#message_message' => $this->t('Upload files will include the file\'s id, name, uri, and data (<a href=":href">Base64</a> encode).', [':href' => 'https://en.wikipedia.org/wiki/Base64']),
        '#message_type' => 'warning',
        '#message_close' => TRUE,
        '#message_id' => 'webform_node.references',
        '#message_storage' => WebformMessage::STORAGE_SESSION,
      ];
    }
    $form['submission_data']['excluded_data'] = [
      '#type' => 'webform_excluded_columns',
      '#title' => $this->t('Posted data'),
      '#title_display' => 'invisible',
      '#webform_id' => $webform->id(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['excluded_data'],
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $code = $this->createAccessCode($webform_submission);
    // Lets also give the user a cookie as a part of redemption.
    $c = \Drupal::request()->cookies->all();
    if (empty($c['Drupal_visitor_flashpoint_access_code'])) {
      user_cookie_save(['flashpoint_access_code' => $code]);
    }
    else {
      $code_concat = implode('|', [$c['Drupal_visitor_flashpoint_access_code'], $code]);
      user_cookie_save(['flashpoint_access_code'=> $code_concat]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
    $this->deleteAccessCode($webform_submission);
  }

  protected function createAccessCode($webform_submission) {
    $query = \Drupal::entityQuery('flashpoint_access_code');
    $access_code = $this->replaceTokens($this->configuration['access_code'], $webform_submission);
    $query->condition('access_code', $access_code);
    $query->condition('group_id', $this->configuration['group_id']);
    $result = $query->execute();
    if (empty($result)) {
      $data = [];
      $data['name'] = $this->replaceTokens($this->configuration['name'], $webform_submission);
      $data['access_code'] = $access_code;
      $data['field_redemption_type'] = $this->replaceTokens($this->configuration['field_redemption_type'], $webform_submission);
      $data['maximum_usage'] = $this->configuration['maximum_usage'];
      $data['group_id'] = $this->configuration['group_id'];
      $code = FlashpointAccessCode::create($data)->save();
    }
    return $access_code;
  }

  protected function deleteAccessCode($webform_submission) {
    $query = \Drupal::entityQuery('flashpoint_access_code');
    $access_code = $this->replaceTokens($this->configuration['access_code'], $webform_submission);
    $query->condition('access_code', $access_code);
    $query->condition('group_id', $this->configuration['group_id']);
    $result = $query->execute();
    if (!empty($result)) {
      $codes = FlashpointAccessCode::loadMultiple($result);
      foreach($codes as $code) {
        $code->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildTokenTreeElement(array $token_types = ['webform', 'webform_submission'], $description = NULL) {
    $description = $description ?: $this->t('Use [webform_submission:values:ELEMENT_KEY:raw] to get plain text values.');
    return parent::buildTokenTreeElement($token_types, $description);
  }

}
