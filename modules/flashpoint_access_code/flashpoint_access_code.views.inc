<?php

/**
 * @file
 * Provide views data for flashpoint_access_code.
 */

/**
 * Implements hook_views_data().
 */
function flashpoint_access_code_views_data() {

  $data['flashpoint_access_code']['access_code_usage_count'] = [
    'title' => t('Access Code Usage'),
    'help' => t('The number of times this code has been used'),
    'field' => [
      'id' => 'access_code_usage_count',
    ],
  ];

  return $data;
}
