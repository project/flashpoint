<?php

/**
 * @file
 * Contains flashpoint_access_code_usage.page.inc.
 *
 * Page callback for Flashpoint access code usage entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Flashpoint access code usage templates.
 *
 * Default template: flashpoint_access_code_usage.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_flashpoint_access_code_usage(array &$variables) {
  // Fetch FlashpointAccessCodeUsage Entity Object.
  $flashpoint_access_code_usage = $variables['elements']['#flashpoint_access_code_usage'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
