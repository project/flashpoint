<?php

namespace Drupal\flashpoint_course_content\Plugin\flashpoint_course_content;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\flashpoint_course_content\FlashpointCourseContentRendererInterface;
use Drupal\flashpoint_course_content\FlashpointCourseContentUtilities;

/**
 * @FlashpointCourseContentRenderer(
 *   id = "flashpoint_course_content_file_link_renderer",
 *   label = @Translation("File Link renderer"),
 * )
 */
class FlashpointCourseContentFileLinkRenderer extends PluginBase implements FlashpointCourseContentRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('File Link Renderer. Renders a link to link directly from the listing. Only works if field_link exists in the content.');
  }

  /**
   * @param $bundle
   */
  public static function getContentRenderData($bundle, $type = 'class') {
    return FlashpointCourseContentDefaultRenderer::getContentRenderData($bundle, $type);
  }

  /**
   * Render course content in a listing context.
   *
   * @param $content
   * @param $account
   * @param bool|string $force_status
   * @return array
   */
  public static function renderListing($content, $account, $force_status = FALSE) {
    $id = $content->id();
    $classes = FlashpointCourseContentLinkRenderer::getContentRenderData($content->bundle(), 'class');
    $icons = FlashpointCourseContentLinkRenderer::getContentRenderData($content->bundle(), 'icon');
    $status = FlashpointCourseContentUtilities::getCourseContentStatus($content, $account, $force_status);

    $content_label = $icons[$status] . ' ' . $content->label();
    if ($status !== 'lock') {
      /* @var $content \Drupal\flashpoint_course_content\Entity\FlashpointCourseContent */
      $content_array = $content->toArray();
      // Set up label as if the field is missing, and override if we have information.
      $content_url = Url::fromRoute('entity.flashpoint_course_content.canonical', ['flashpoint_course_content' => $content->id()]);
      $content_label = Link::fromTextAndUrl($content->label(), $content_url)->toString()->getGeneratedLink();
      if ($content->hasField('field_downloadable_file')) {
        $content_array = $content->toArray();
        if (isset($content_array['field_downloadable_file'][0]['target_id']) && !empty($content_array['field_downloadable_file'][0]['target_id'])) {
          $file = File::load($content_array['field_downloadable_file'][0]['target_id']);
          $content_url = Url::fromUserInput($file->createFileUrl());
          $content_label = Link::fromTextAndUrl($content->label(), $content_url)->toString()->getGeneratedLink();
        }
      }
    }

    $ret = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['flashpoint-content', $classes[$status]],
      ],
      'label' => [
        '#type' => 'html_tag',
        '#tag' => 'h4',
        '#value' => $icons[$status] . ' ' . $content_label,
        '#attributes' => [
          'class' => ['flashpoint-content-label'],
        ],
      ],
    ];
    return $ret;
  }

  /**
   * @param $course
   * @return array
   */
  public function renderProgressButtons($content, $course) {
    return FlashpointCourseContentDefaultRenderer::renderProgressButtons($content, $course);
  }

}
