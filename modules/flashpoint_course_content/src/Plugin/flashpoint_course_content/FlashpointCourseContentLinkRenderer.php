<?php

namespace Drupal\flashpoint_course_content\Plugin\flashpoint_course_content;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\flashpoint_course_content\FlashpointCourseContentRendererInterface;
use Drupal\flashpoint_course_content\FlashpointCourseContentUtilities;

/**
 * @FlashpointCourseContentRenderer(
 *   id = "flashpoint_course_content_link_renderer",
 *   label = @Translation("Link renderer"),
 * )
 */
class FlashpointCourseContentLinkRenderer extends PluginBase implements FlashpointCourseContentRendererInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Link Renderer. Renders a link to link directly from the listing. Only works if field_link exists in the content.');
  }

  /**
   * @param $bundle
   * @param string $type
   * @return array
   */
  public static function getContentRenderData($bundle, $type = 'class') {
    return FlashpointCourseContentDefaultRenderer::getContentRenderData($bundle, $type);
  }

  /**
   * Render course content in a listing context.
   *
   * @param $content
   * @param $account
   * @param bool|string $force_status
   * @return array
   */
  public static function renderListing($content, $account, $force_status = FALSE) {
    $id = $content->id();
    $classes = FlashpointCourseContentLinkRenderer::getContentRenderData($content->bundle(), 'class');
    $icons = FlashpointCourseContentLinkRenderer::getContentRenderData($content->bundle(), 'icon');
    $status = FlashpointCourseContentUtilities::getCourseContentStatus($content, $account, $force_status);

    $content_label = $icons[$status] . ' ' . $content->label();
    if ($status !== 'lock') {
      /* @var $content \Drupal\flashpoint_course_content\Entity\FlashpointCourseContent */
      // Set up label as if the field is missing, and override if we have information.
      $content_url = Url::fromRoute('entity.flashpoint_course_content.canonical', ['flashpoint_course_content' => $content->id()]);
      $content_label = Link::fromTextAndUrl($content->label(), $content_url)->toString()->getGeneratedLink();
      if ($content->hasField('field_link')) {
        $content_array = $content->toArray();
        if (isset($content_array['field_link'][0]['uri']) && isset($content_array['field_link'][0]['title']) &&
          !empty($content_array['field_link'][0]['uri']) && !empty($content_array['field_link'][0]['title'])) {
          $content_url = Url::fromUri($content_array['field_link'][0]['uri']);
          $content_label = Link::fromTextAndUrl($content_array['field_link'][0]['title'], $content_url)->toString()->getGeneratedLink();
        }
        elseif (isset($content_array['field_link'][0]['uri']) && !empty($content_array['field_link'][0]['uri'])) {
          $content_url = Url::fromUri($content_array['field_link'][0]['uri']);
          $content_label = Link::fromTextAndUrl($content->label(), $content_url)->toString()->getGeneratedLink();
        }
      }
    }

    $ret = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['flashpoint-content', $classes[$status]],
      ],
      'label' => [
        '#type' => 'html_tag',
        '#tag' => 'h4',
        '#value' => $icons[$status] . ' ' . $content_label,
        '#attributes' => [
          'class' => ['flashpoint-content-label'],
        ],
      ],
    ];
    return $ret;
  }

  /**
   * @param $content
   * @param $course
   * @return array
   */
  public function renderProgressButtons($content, $course) {
    return FlashpointCourseContentDefaultRenderer::renderProgressButtons($content, $course);
  }

}
