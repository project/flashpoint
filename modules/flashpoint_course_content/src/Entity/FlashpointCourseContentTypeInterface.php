<?php

namespace Drupal\flashpoint_course_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Flashpoint course content type entities.
 */
interface FlashpointCourseContentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
