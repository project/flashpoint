<?php

/**
 * @file
 */

namespace Drupal\flashpoint_course_content;

class FlashpointCourseContentUtilities {

  public static function getCourseContentStatus($content, $account, $force_status = FALSE) {
    $status = 'neutral';
    if (in_array($force_status, ['neutral', 'lock', 'passed', 'pending'])) {
      $status = $force_status;
    }
    else {
      if (!$content->isNeutral($account)) {
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists('flashpoint_lrs_client')) {
          $lock_status = $content->isLocked($account);
          $pass_status = $content->isPassed($account);
          $status = $pass_status ? 'passed' : 'pending';
          $status = $lock_status ? 'lock' : $status;
        }
      }
    }

    return $status;
  }

}
