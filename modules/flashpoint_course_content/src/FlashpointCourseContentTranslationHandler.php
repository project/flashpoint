<?php

namespace Drupal\flashpoint_course_content;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for flashpoint_course_content.
 */
class FlashpointCourseContentTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
