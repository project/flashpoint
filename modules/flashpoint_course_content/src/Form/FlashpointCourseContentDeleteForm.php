<?php

namespace Drupal\flashpoint_course_content\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Flashpoint course content entities.
 *
 * @ingroup flashpoint_course_content
 */
class FlashpointCourseContentDeleteForm extends ContentEntityDeleteForm {


}
