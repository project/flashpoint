<?php

namespace Drupal\flashpoint\Plugin\flashpoint_access;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;

/**
 * @FlashpointAccessMethod(
 *   id = "free_access",
 *   label = @Translation("Free Access with message hidden")
 * )
 */
class FreeAccess extends PluginBase implements FlashpointAccessMethodInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('This allows a course to be offered for free.');
  }

  /**
   * @param Group $group
   * @param AccountInterface $account
   *
   * Determines whether someone has access to preview a group
   * @return boolean
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Free Access courses may always be previewed.
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    // Free Access courses may always be viewed.
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {
    // Free Access courses may always be accessed.
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    // With Free Access, they can always view the group, so no message is needed.
    return [];
  }

  /**
   * @inheritdoc
   * @TODO create a more robust means of specifying button text.
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $type = $group->bundle() === 'flashpoint_course' ? 'Enroll' : 'Join';
    $element = [];
    return $element;
  }
}
