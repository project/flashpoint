<?php

namespace Drupal\flashpoint\Plugin\flashpoint_access;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flashpoint\FlashpointAccessMethodInterface;
use Drupal\group\Entity\Group;


/**
 * @FlashpointAccessMethod(
 *   id = "manual_access_with_message",
 *   label = @Translation("Manual Access: Show a message")
 * )
 */
class ManualAccessWithMessage extends PluginBase implements FlashpointAccessMethodInterface {

  /**
   * @inheritdoc
   */
  public function description()
  {
    return $this->t('Users must be manually approved by an administrator,
    which means memberships must be created manually.');
  }

  /**
   * @inheritdoc
   */
  public static function checkAccess(Group $group, AccountInterface $account) {
    // The person must already be a group member, and cannot enroll on their own.
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account) {
    // Manual Access courses may not be previewed if the person is not a member.
    $memberships = \Drupal::service('group.membership_loader')->load($group, $account);

    if(!empty($memberships)) {
      return TRUE;
    }
    else {
      // They are not a member of this group.
      return FALSE;
    }
  }

  /**
   * @inheritdoc
   */
  public static function checkViewAccess(Group $group, AccountInterface $account) {
    // View access and preview access use the same logic.
    $access = ManualAccess::checkPreviewAccess($group, $account);
    return $access;
  }

  /**
   * @inheritdoc
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account) {
    // With Manual Access, you have to have joined, and view only is not an option.
    return [];
  }

  /**
   * @inheritdoc
   * @TODO create a more robust means of specifying button text.
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account) {
    $type = $group->getGroupType()->label();
    $element = [
      'manual_access' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => t('Manual Access: You must be approved by an administrator for this %type.', ['%type' => $type]),
      ],
    ];
    return $element;
  }
}
