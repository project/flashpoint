<?php
/**
 * @file
 * Provides Drupal\flashpoint\FlashpointAccessMethodInterface
 */
namespace Drupal\flashpoint;

use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Group;

/**
 * An interface for all MyPlugin type plugins.
 */
interface FlashpointAccessMethodInterface {
  /**
   * Provide a description of the plugin.
   * @return string
   *   A string description of the plugin.
   */
  public function description();

  /**
   * Determines whether someone has access to enroll in/join a group.
   *
   * Legacy note: We looked at join only before, view/preview access was set up later.
   * That is why this isn't called 'checkJoinAccess.'
   * @param Group $group
   * @param AccountInterface $account
   *
   * @return boolean
   */
  public static function checkAccess(Group $group, AccountInterface $account);

  /**
   * Determines whether someone has access to preview a group
   * @param Group $group
   * @param AccountInterface $account
   *
   * @return boolean
   */
  public static function checkPreviewAccess(Group $group, AccountInterface $account);

  /**
   * Determines whether someone has access to preview a group
   * @param Group $group
   * @param AccountInterface $account
   *
   * @return boolean
   */
  public static function checkViewAccess(Group $group, AccountInterface $account);

  /**
   * Description of how to view the group content, and possible actions.
   * @param Group $group
   * @param AccountInterface $account
   * @return mixed
   */
  public static function viewAccessFormElement(Group $group, AccountInterface $account);

  /**
   * Description of how to join the group, and possible actions.
   * @param Group $group
   * @return array
   */
  public static function joinAccessFormElement(Group $group, AccountInterface $account);
}